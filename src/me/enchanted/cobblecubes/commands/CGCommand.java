package me.enchanted.cobblecubes.commands;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.enchanted.cobblecubes.utils.ItemStacks;
import me.enchanted.cobblecubes.utils.Messages;

public class CGCommand implements CommandExecutor{
	
    public boolean onCommand(final CommandSender sender, final org.bukkit.command.Command command, final String label, final String[] args) {
        if (sender instanceof Player) {
            final Player p = (Player)sender;
            if (p.hasPermission("cg.*")) {
                if (args.length == 3) {
                    if (args[0].equalsIgnoreCase("3x3")) {
                    	int loot = Integer.parseInt(args[1]);
                    	int quantity = Integer.parseInt(args[2]);
                        p.getInventory().addItem(new ItemStack[] { ItemStacks.getItemStack3X3(loot, quantity) });
                    }
                    else if (args[0].equalsIgnoreCase("5x5")) {
                    	int loot = Integer.parseInt(args[1]);
                    	int quantity = Integer.parseInt(args[2]);
                        p.getInventory().addItem(new ItemStack[] { ItemStacks.getItemStack5X5(loot, quantity) });
                    }
                    else if (args[0].equalsIgnoreCase("7x7")) {
                    	int loot = Integer.parseInt(args[1]);
                    	int quantity = Integer.parseInt(args[2]);
                        p.getInventory().addItem(new ItemStack[] { ItemStacks.getItemStack7X7(loot, quantity) });
                    } 
                    else if (args[0].equalsIgnoreCase("9x9")) {
                    	int loot = Integer.parseInt(args[1]);
                    	int quantity = Integer.parseInt(args[2]);
                        p.getInventory().addItem(new ItemStack[] { ItemStacks.getItemStack9X9(loot, quantity) });
                    } 
                    else if (args[0].equalsIgnoreCase("11x11")) {
                    	int loot = Integer.parseInt(args[1]);
                    	int quantity = Integer.parseInt(args[2]);
                        p.getInventory().addItem(new ItemStack[] { ItemStacks.getItemStack11X11(loot, quantity) });
                    } 
                    else if (args[0].equalsIgnoreCase("13x13")) {
                    	int loot = Integer.parseInt(args[1]);
                    	int quantity = Integer.parseInt(args[2]);
                        p.getInventory().addItem(new ItemStack[] { ItemStacks.getItemStack13X13(loot, quantity) });
                    } else {
                        Messages.NOARGUMENTS.send(p);
                    }
                }
                else {
                    Messages.NOARGUMENTS.send(p);
                }
            }
            else {
                Messages.NOPERMMISION.send(p);
            }
        }
        return true;
    }
}
