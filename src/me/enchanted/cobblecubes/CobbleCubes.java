package me.enchanted.cobblecubes;

import org.bukkit.plugin.java.*;
import org.bukkit.scheduler.BukkitRunnable;

import me.enchanted.cobblecubes.commands.*;
import me.enchanted.cobblecubes.guis.LootGui;
import me.enchanted.cobblecubes.guis.StorageGui;
import me.enchanted.cobblecubes.guis.UpgradesGui;
import me.enchanted.cobblecubes.listeners.BreakBlockEvent;
import me.enchanted.cobblecubes.listeners.PlaceGeneratorEvent;
import me.enchanted.cobblecubes.utils.Glow;
import me.enchanted.cobblecubes.utils.PlayerData;
import me.enchanted.cobblecubes.utils.Randomize;
import net.milkbowl.vault.economy.Economy;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;

import org.bukkit.command.*;
import org.bukkit.*;
import org.bukkit.event.*;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.plugin.*;
import org.bukkit.configuration.file.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;

public class CobbleCubes extends JavaPlugin {
	
	private static CobbleCubes instance;
	private File datafile;
	private FileConfiguration data;
	public HashMap<UUID, ArrayList<Location>> allGeneratorBlocks = new HashMap<UUID, ArrayList<Location>>();
	public HashMap<UUID, ArrayList<Location>> allBorderBlocks = new HashMap<UUID, ArrayList<Location>>();
	public static Economy econ = null;

	public void onEnable() {
		(CobbleCubes.instance = this).loadConfigurations();
		if (!setupEconomy()) {
			Bukkit.getServer().getConsoleSender().sendMessage(
					String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
		this.getCommand("cg").setExecutor((CommandExecutor) new CGCommand());
		Bukkit.getServer().getPluginManager().registerEvents((Listener) new PlaceGeneratorEvent(), (Plugin) this);
		Bukkit.getServer().getPluginManager().registerEvents((Listener) new BreakBlockEvent(), (Plugin) this);
		Bukkit.getServer().getPluginManager().registerEvents((Listener) new StorageGui(), (Plugin) this);
		Bukkit.getServer().getPluginManager().registerEvents((Listener) new LootGui(), (Plugin) this);
		Bukkit.getServer().getPluginManager().registerEvents((Listener) new UpgradesGui(), (Plugin) this);
		setupRegen();
		registerGlow();
	}

	public void onDisable() {
		CobbleCubes.instance = null;
	}

	public static CobbleCubes getInstance() {
		return CobbleCubes.instance;
	}

	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
		

	}

	private void loadConfigurations() {
		this.datafile = new File(this.getDataFolder() + "\\data.yml");
		this.data = (FileConfiguration) YamlConfiguration.loadConfiguration(this.datafile);
		this.saveDefaultConfig();
	}

	public FileConfiguration getDataConfig() {
		return this.data;
	}

	private void registerGlow() {
		try {
			Field f = Enchantment.class.getDeclaredField("acceptingNew");
			f.setAccessible(true);
			f.set(null, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			Glow glow = new Glow(120);
			Enchantment.registerEnchantment(glow);
		} catch (IllegalArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setupRegen() {
		new BukkitRunnable() {

			int gBL = 0;

			public void run() {
				try {
					for (Player p : Bukkit.getOnlinePlayers()) {

						ArrayList<Location> air = new ArrayList<Location>();
						PlayerData data = new PlayerData(p.getUniqueId());
						air.clear();
						for (Location l : allGeneratorBlocks.get(p.getUniqueId())) {
							if (l.getBlock().getType().equals(Material.AIR)) {
								air.add(l);
							}
						}
						gBL = air.size() / 2;
						System.out.print(gBL);
						for (int g = 0; g <= gBL; g++) {
							Material rB = null;
							for (String keys : data.getConfig().getConfigurationSection("gens").getKeys(false)) {

								if (PlaceGeneratorEvent.center.get(air.get(g))
										.equals(new Location(
												Bukkit.getWorld(
														data.getConfig().getString("gens." + keys + ".center.world")),
												data.getConfig().getInt("gens." + keys + ".center.x"),
												data.getConfig().getInt("gens." + keys + ".center.y"),
												data.getConfig().getInt("gens." + keys + ".center.z")))) {

									rB = PlayerData.getRandomBlock(data.getConfig().getInt("gens." + keys + ".loot"));
								}
							}
							air.get(g).getBlock().setType(rB);
							air.remove(g);
						}
					}
				} catch (Exception e) {
					return;
				}

			}
		}.runTaskTimer((Plugin) CobbleCubes.getInstance(),
				150,
				150);

	}
}
