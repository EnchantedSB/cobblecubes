package me.enchanted.cobblecubes.listeners;

import org.bukkit.event.block.*;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.*;

import me.enchanted.cobblecubes.CobbleCubes;
import me.enchanted.cobblecubes.utils.ItemStacks;
import me.enchanted.cobblecubes.utils.Messages;
import me.enchanted.cobblecubes.utils.PlayerData;
import me.enchanted.cobblecubes.utils.Randomize;
import org.bukkit.entity.*;
import org.bukkit.block.*;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.event.*;
import org.bukkit.scheduler.BukkitRunnable;

public class PlaceGeneratorEvent implements Listener {

	public static HashMap<Location, Location> center = new HashMap<Location, Location>();
	public static HashMap<Location, String> type = new HashMap<Location, String>();
	public static HashMap<Player, Location> pCenter = new HashMap<Player, Location>();
	public static HashMap<UUID, Integer> gens = new HashMap<UUID, Integer>();
	ArrayList<Location> generating = new ArrayList<Location>();

	@EventHandler
	public void createPD(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		PlayerData data = new PlayerData(p.getUniqueId());

		if (!data.exists()) {
			try {
				File dir = new File(CobbleCubes.getInstance().getDataFolder() + File.separator + "CubeData");
				File file = new File(CobbleCubes.getInstance().getDataFolder() + File.separator + "CubeData",
						p.getUniqueId().toString() + ".yml");
				dir.mkdirs();
				file.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else {
			for (String cf : data.getConfig().getConfigurationSection("gens").getKeys(false)) {
				ArrayList<Location> l = new ArrayList<Location>();
				World w = Bukkit.getWorld(data.getConfig().getString("gens." + cf + ".center.world"));
				int tx = data.getConfig().getInt("gens." + cf + ".center.x");
				int ty = data.getConfig().getInt("gens." + cf + ".center.y");
				int tz = data.getConfig().getInt("gens." + cf + ".center.z");
				Location tcenter = new Location(w, tx, ty, tz);
				if (data.getConfig().getString("gens." + cf + ".type").equalsIgnoreCase("3x3")) {
					for (int x = tcenter.getBlockX() - 1; x <= tcenter.getBlockX() + 1; ++x) {
						for (int y = tcenter.getBlockY() - 1; y <= tcenter.getBlockY() + 1; ++y) {
							for (int z = tcenter.getBlockZ() - 1; z <= tcenter.getBlockZ() + 1; ++z) {
								
								center.put(new Location(w, x, y, z), tcenter);
								type.put(new Location(w, x, y, z), data.getConfig().getString("gens." + cf + ".type"));
								l.add(new Location(w, x, y, z));
							}
						}
					}
					CobbleCubes.getInstance().allGeneratorBlocks.put(p.getUniqueId(), l);
				} else if (data.getConfig().getString("gens." + cf + ".type").equalsIgnoreCase("5x5")) {
					for (int x = tcenter.getBlockX() - 2; x <= tcenter.getBlockX() + 2; ++x) {
						for (int y = tcenter.getBlockY() - 2; y <= tcenter.getBlockY() + 2; ++y) {
							for (int z = tcenter.getBlockZ() - 2; z <= tcenter.getBlockZ() + 2; ++z) {
								
								center.put(new Location(w, x, y, z), tcenter);
								type.put(new Location(w, x, y, z), data.getConfig().getString("gens." + cf + ".type"));
								l.add(new Location(w, x, y, z));
							}
						}
					}
					CobbleCubes.getInstance().allGeneratorBlocks.put(p.getUniqueId(), l);
				} else if (data.getConfig().getString("gens." + cf + ".type").equalsIgnoreCase("7x7")) {
					for (int x = tcenter.getBlockX() - 3; x <= tcenter.getBlockX() + 3; ++x) {
						for (int y = tcenter.getBlockY() - 3; y <= tcenter.getBlockY() + 3; ++y) {
							for (int z = tcenter.getBlockZ() - 3; z <= tcenter.getBlockZ() + 3; ++z) {
								
								center.put(new Location(w, x, y, z), tcenter);
								type.put(new Location(w, x, y, z), data.getConfig().getString("gens." + cf + ".type"));
								l.add(new Location(w, x, y, z));
							}
						}
					}
					CobbleCubes.getInstance().allGeneratorBlocks.put(p.getUniqueId(), l);
				} else if (data.getConfig().getString("gens." + cf + ".type").equalsIgnoreCase("9x9")) {
					for (int x = tcenter.getBlockX() - 4; x <= tcenter.getBlockX() + 4; ++x) {
						for (int y = tcenter.getBlockY() - 4; y <= tcenter.getBlockY() + 4; ++y) {
							for (int z = tcenter.getBlockZ() - 4; z <= tcenter.getBlockZ() + 4; ++z) {
								
								center.put(new Location(w, x, y, z), tcenter);
								type.put(new Location(w, x, y, z), data.getConfig().getString("gens." + cf + ".type"));
								l.add(new Location(w, x, y, z));
							}
						}
					}
					CobbleCubes.getInstance().allGeneratorBlocks.put(p.getUniqueId(), l);
				} else if (data.getConfig().getString("gens." + cf + ".type").equalsIgnoreCase("11x11")) {
					for (int x = tcenter.getBlockX() - 5; x <= tcenter.getBlockX() + 5; ++x) {
						for (int y = tcenter.getBlockY() - 5; y <= tcenter.getBlockY() + 5; ++y) {
							for (int z = tcenter.getBlockZ() - 5; z <= tcenter.getBlockZ() + 5; ++z) {
								
								center.put(new Location(w, x, y, z), tcenter);
								type.put(new Location(w, x, y, z), data.getConfig().getString("gens." + cf + ".type"));
								l.add(new Location(w, x, y, z));
							}
						}
					}
					CobbleCubes.getInstance().allGeneratorBlocks.put(p.getUniqueId(), l);
				} else if (data.getConfig().getString("gens." + cf + ".type").equalsIgnoreCase("13x13")) {
					for (int x = tcenter.getBlockX() - 6; x <= tcenter.getBlockX() + 6; ++x) {
						for (int y = tcenter.getBlockY() - 6; y <= tcenter.getBlockY() + 6; ++y) {
							for (int z = tcenter.getBlockZ() - 6; z <= tcenter.getBlockZ() + 6; ++z) {
								
								center.put(new Location(w, x, y, z), tcenter);
								type.put(new Location(w, x, y, z), data.getConfig().getString("gens." + cf + ".type"));
								l.add(new Location(w, x, y, z));
							}
						}
					}
					CobbleCubes.getInstance().allGeneratorBlocks.put(p.getUniqueId(), l);
				}
			}
		}
	}

	@EventHandler
	public void onInteractGen(PlayerInteractEvent e) {
		try {
			if (!e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
				return;
			if (CobbleCubes.getInstance().allGeneratorBlocks.get(e.getPlayer().getUniqueId())
					.contains(e.getClickedBlock().getLocation())) {
				pCenter.put(e.getPlayer(), PlaceGeneratorEvent.center.get(e.getClickedBlock().getLocation()));
				if (center.containsKey(e.getClickedBlock().getLocation())) {
					if (type.get(e.getClickedBlock().getLocation()).equalsIgnoreCase("3x3")) {
						if (generating.contains(center.get(e.getClickedBlock().getLocation()))) {
							e.getPlayer().sendMessage("�cThe Cobble Cube is still generating. Please wait.");
							return;
						}
						BreakBlockEvent.openInv(e.getPlayer(), center.get(e.getPlayer()));
						/*
						 * removeACube(center.get(e.getClickedBlock().getLocation()));
						 * e.getPlayer().getInventory().addItem(new ItemStack[] {
						 * ItemStacks.getItemStack3X3() });
						 */
					} else if (type.get(e.getClickedBlock().getLocation()).equalsIgnoreCase("5x5")) {
						if (generating.contains(center.get(e.getClickedBlock().getLocation()))) {
							e.getPlayer().sendMessage("�cThe Cobble Cube is still generating. Please wait.");
							return;
						}
						BreakBlockEvent.openInv(e.getPlayer(), center.get(e.getClickedBlock().getLocation()));
						/*
						 * removeACube(center.get(e.getClickedBlock().getLocation()));
						 * e.getPlayer().getInventory().addItem(new ItemStack[] {
						 * ItemStacks.getItemStack3X3() });
						 */
					} else if (type.get(e.getClickedBlock().getLocation()).equalsIgnoreCase("7x7")) {
						if (generating.contains(center.get(e.getClickedBlock().getLocation()))) {
							e.getPlayer().sendMessage("�cThe Cobble Cube is still generating. Please wait.");
							return;
						}
						BreakBlockEvent.openInv(e.getPlayer(), center.get(e.getClickedBlock().getLocation()));
						/*
						 * removeACube(center.get(e.getClickedBlock().getLocation()));
						 * e.getPlayer().getInventory().addItem(new ItemStack[] {
						 * ItemStacks.getItemStack3X3() });
						 */
					} else if (type.get(e.getClickedBlock().getLocation()).equalsIgnoreCase("9x9")) {
						if (generating.contains(center.get(e.getClickedBlock().getLocation()))) {
							e.getPlayer().sendMessage("�cThe Cobble Cube is still generating. Please wait.");
							return;
						}
						BreakBlockEvent.openInv(e.getPlayer(), center.get(e.getClickedBlock().getLocation()));
						/*
						 * removeACube(center.get(e.getClickedBlock().getLocation()));
						 * e.getPlayer().getInventory().addItem(new ItemStack[] {
						 * ItemStacks.getItemStack3X3() });
						 */
					} else if (type.get(e.getClickedBlock().getLocation()).equalsIgnoreCase("11x11")) {
						if (generating.contains(center.get(e.getClickedBlock().getLocation()))) {
							e.getPlayer().sendMessage("�cThe Cobble Cube is still generating. Please wait.");
							return;
						}
						BreakBlockEvent.openInv(e.getPlayer(), center.get(e.getClickedBlock().getLocation()));
						/*
						 * removeACube(center.get(e.getClickedBlock().getLocation()));
						 * e.getPlayer().getInventory().addItem(new ItemStack[] {
						 * ItemStacks.getItemStack3X3() });
						 */
					} else if (type.get(e.getClickedBlock().getLocation()).equalsIgnoreCase("13x13")) {
						if (generating.contains(center.get(e.getClickedBlock().getLocation()))) {
							e.getPlayer().sendMessage("�cThe Cobble Cube is still generating. Please wait.");
							return;
						}
						BreakBlockEvent.openInv(e.getPlayer(), center.get(e.getClickedBlock().getLocation()));
						/*
						 * removeACube(center.get(e.getClickedBlock().getLocation()));
						 * e.getPlayer().getInventory().addItem(new ItemStack[] {
						 * ItemStacks.getItemStack3X3() });
						 */
					}
				}
			}
		} catch (Exception ex) {
			return;
		}
	}

	final HashMap<UUID, ArrayList<Location>> newLocations = new HashMap<UUID, ArrayList<Location>>();

	@EventHandler
	public void onPlaceGenerator(final BlockPlaceEvent e) {
		final Player player = e.getPlayer();
		if (newLocations.get(player.getUniqueId()) == null) {
			newLocations.put(player.getUniqueId(), new ArrayList<Location>());
		}
		if (newLocations.get(player.getUniqueId()).size() != 0 && (e.getItemInHand().getItemMeta().getLore().get(2)
				.equalsIgnoreCase(" �6�l* �e�lSize: �r�l3x3")
				|| e.getItemInHand().getItemMeta().getLore().get(2).equalsIgnoreCase(" �6�l* �e�lSize: �r�l5x5")
				|| e.getItemInHand().getItemMeta().getLore().get(2).equalsIgnoreCase(" �6�l* �e�lSize: �r�l7x7")
				|| e.getItemInHand().getItemMeta().getLore().get(2).equalsIgnoreCase(" �6�l* �e�lSize: �r�l9x9")
				|| e.getItemInHand().getItemMeta().getLore().get(2).equalsIgnoreCase(" �6�l* �e�lSize: �r�l11x11")
				|| e.getItemInHand().getItemMeta().getLore().get(2).equalsIgnoreCase(" �6�l* �e�lSize: �r�l13x13")
				|| e.getItemInHand().getItemMeta().getLore().get(2).equalsIgnoreCase(" �6�l* �e�lSize: �r�l15x15"))) {
			player.sendMessage("�cYou already have a CobbleCube generating, please wait!");
			e.setCancelled(true);
			return;
		}
		PlayerData data = new PlayerData(player.getUniqueId());
		if (e.getItemInHand().getItemMeta().getLore().get(2).equalsIgnoreCase(" �6�l* �e�lSize: �r�l3x3")) {
			e.setCancelled(true);
			final Block blockplaced = e.getBlockPlaced();
			if (player.getLocation().distance(blockplaced.getLocation().add(0.5, 0.0, 0.5)) >= 2.2) {

				blockplaced.getLocation().add(0, 1, 0);
				boolean r2 = true;
				boolean ready = true;
				Label_0336: for (int x = blockplaced.getLocation().getBlockX() - 2; x <= blockplaced.getLocation()
						.getBlockX() + 2; ++x) {
					for (int y = blockplaced.getLocation().getBlockY(); y <= blockplaced.getLocation().getBlockY()
							+ 5; ++y) {
						for (int z = blockplaced.getLocation().getBlockZ() - 2; z <= blockplaced.getLocation()
								.getBlockZ() + 2; ++z) {
							if (blockplaced.getWorld().getBlockAt(x, y, z).getType() != Material.AIR) {
								if (!blockplaced.getLocation().equals((Object) new Location(blockplaced.getWorld(),
										(double) x, (double) y, (double) z))) {
									ready = false;
									Messages.NOTENOUGHSPACE.send(player);
									e.setCancelled(true);
									break Label_0336;
								}
							}
						}
					}
				}
				if (!r2)
					return;
				Label_2591: for (int x = blockplaced.getLocation().getBlockX() - 1; x <= blockplaced.getLocation()
						.getBlockX() + 1; ++x) {
					for (int y = blockplaced.getLocation().add(0, 1, 0).getBlockY(); y <= blockplaced.getLocation()
							.getBlockY() + 3; ++y) {
						for (int z = blockplaced.getLocation().getBlockZ() - 1; z <= blockplaced.getLocation()
								.getBlockZ() + 1; ++z) {
							if (blockplaced.getWorld().getBlockAt(x, y, z).getType() == Material.AIR
									|| blockplaced.getWorld().getBlockAt(x, y, z) == null) {
								newLocations.get(player.getUniqueId())
										.add(new Location(blockplaced.getWorld(), (double) x, (double) y, (double) z));
							} else {
								if (!blockplaced.getLocation().equals((Object) new Location(blockplaced.getWorld(),
										(double) x, (double) y, (double) z))) {
									ready = false;
									Messages.NOTENOUGHSPACE.send(player);
									e.setCancelled(true);
									newLocations.get(player.getUniqueId()).clear();
									break Label_2591;
								}
							}
						}
					}
				}
				if (ready) {
					Messages.SUCCESSFULLYPLACED.send(player);
					aBorder(player, blockplaced.getLocation());
					generating.add(blockplaced.getLocation().clone().add(0, 2, 0));
					List<Material> mats = new ArrayList<Material>();
					Location center = blockplaced.getLocation().clone().add(0, 2, 0);
					data.createGenerator("3x3", blockplaced.getLocation().clone().add(0, 2, 0),
							getMeta(e.getItemInHand(), "quantity"), getMeta(e.getItemInHand(), "loot"), getMeta(e.getItemInHand(), "multiplier"));
					for (String keys : data.getConfig().getConfigurationSection("gens").getKeys(false)) {
						if (data.getConfig().getInt("gens." + keys + ".center.x") == center.getX()
								&& data.getConfig().getInt("gens." + keys + ".center.y") == center.getY()
								&& data.getConfig().getInt("gens." + keys + ".center.z") == center.getZ()) {
							mats = Randomize.get27RandomBlocks(data.getConfig().getInt("gens." + keys + ".loot"));
						}
					}

					final List<Material> materials = mats;
					int task = new BukkitRunnable() {
						Player p = player;

						@SuppressWarnings("serial")
						@Override
						public void run() {
							if (newLocations.get(p.getUniqueId()).size() == 0) {
								generating.remove(blockplaced.getLocation().clone().add(0, 2, 0));
								System.out.print(getMeta(e.getItemInHand(), "quantity"));
								cancel();
								p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
								new BukkitRunnable() {

									@Override
									public void run() {
										p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);

									}
								}.runTaskLater(CobbleCubes.getInstance(), 6);
								new BukkitRunnable() {

									@Override
									public void run() {
										p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);

									}
								}.runTaskLater(CobbleCubes.getInstance(), 6);
							}
							Random r = new Random();
							final Location newLocation = newLocations.get(p.getUniqueId())
									.get(r.nextInt(newLocations.get(p.getUniqueId()).size()));
							final Material material = materials.get(0);
							type.put(newLocation, "3x3");
							PlaceGeneratorEvent.center.put(newLocation, blockplaced.getLocation().add(0, 2, 0));
							newLocation.getBlock().setType(material);

							materials.remove(0);
							if (CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId()) == null) {
								CobbleCubes.getInstance().allGeneratorBlocks.put(p.getUniqueId(),
										new ArrayList<Location>() {
											{
												add(newLocation);
											}
										});
							} else {
								CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId()).add(newLocation);
							}
							newLocations.get(p.getUniqueId()).remove(newLocation);
						}
					}.runTaskTimer(CobbleCubes.getInstance(), 25, 25).getTaskId();
					gens.put(player.getUniqueId(), task);

					if (e.getItemInHand().getAmount() > 1) {
						player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
					} else {
						player.setItemInHand(new ItemStack(Material.AIR));
					}
				}
			} else {
				Messages.TOCLOSETOPLACE.send(player);
				e.setCancelled(true);
			}
		} else if (e.getItemInHand().getItemMeta().getLore().get(2).equalsIgnoreCase(" �6�l* �e�lSize: �r�l5x5")) {
			e.setCancelled(true);
			final Block blockplaced = e.getBlockPlaced();
			if (player.getLocation().distance(blockplaced.getLocation().add(0.5, 0.0, 0.5)) >= 2.2) {

				blockplaced.getLocation().add(0, 1, 0);
				boolean r2 = true;
				boolean ready = true;
				Label_0336: for (int x = blockplaced.getLocation().getBlockX() - 4; x <= blockplaced.getLocation()
						.getBlockX() + 3; ++x) {
					for (int y = blockplaced.getLocation().getBlockY(); y <= blockplaced.getLocation().getBlockY()
							+ 7; ++y) {
						for (int z = blockplaced.getLocation().getBlockZ() - 3; z <= blockplaced.getLocation()
								.getBlockZ() + 3; ++z) {
							if (blockplaced.getWorld().getBlockAt(x, y, z).getType() != Material.AIR) {
								if (!blockplaced.getLocation().equals((Object) new Location(blockplaced.getWorld(),
										(double) x, (double) y, (double) z))) {
									ready = false;
									Messages.NOTENOUGHSPACE.send(player);
									e.setCancelled(true);
									break Label_0336;
								}
							}
						}
					}
				}
				if (!r2)
					return;
				Label_2591: for (int x = blockplaced.getLocation().getBlockX() - 2; x <= blockplaced.getLocation()
						.getBlockX() + 2; ++x) {
					for (int y = blockplaced.getLocation().add(0, 1, 0).getBlockY(); y <= blockplaced.getLocation()
							.getBlockY() + 5; ++y) {
						for (int z = blockplaced.getLocation().getBlockZ() - 2; z <= blockplaced.getLocation()
								.getBlockZ() + 2; ++z) {
							if (blockplaced.getWorld().getBlockAt(x, y, z).getType() == Material.AIR
									|| blockplaced.getWorld().getBlockAt(x, y, z) == null) {
								newLocations.get(player.getUniqueId())
										.add(new Location(blockplaced.getWorld(), (double) x, (double) y, (double) z));
							} else {
								if (!blockplaced.getLocation().equals((Object) new Location(blockplaced.getWorld(),
										(double) x, (double) y, (double) z))) {
									ready = false;
									Messages.NOTENOUGHSPACE.send(player);
									e.setCancelled(true);
									newLocations.get(player.getUniqueId()).clear();
									break Label_2591;
								}
							}
						}
					}
				}
				if (ready) {
					bBorder(player, blockplaced.getLocation());
					Messages.SUCCESSFULLYPLACED.send(player);
					generating.add(blockplaced.getLocation().clone().add(0, 3, 0));
					List<Material> mats = new ArrayList<Material>();
					Location center = blockplaced.getLocation().clone().add(0, 3, 0);
					data.createGenerator("5x5", blockplaced.getLocation().clone().add(0, 3, 0),
							getMeta(e.getItemInHand(), "quantity"), getMeta(e.getItemInHand(), "loot"), getMeta(e.getItemInHand(), "multiplier"));
					for (String keys : data.getConfig().getConfigurationSection("gens").getKeys(false)) {
						if (data.getConfig().getInt("gens." + keys + ".center.x") == center.getX()
								&& data.getConfig().getInt("gens." + keys + ".center.y") == center.getY()
								&& data.getConfig().getInt("gens." + keys + ".center.z") == center.getZ()) {
							mats = Randomize.get125RandomBlocks(data.getConfig().getInt("gens." + keys + ".loot"));
						}
					}

					final List<Material> materials = mats;
					int task = new BukkitRunnable() {

						Player p = player;

						@SuppressWarnings("serial")
						@Override
						public void run() {
							if (newLocations.get(p.getUniqueId()).size() == 0) {
								generating.remove(blockplaced.getLocation().clone().add(0, 3, 0));
								System.out.print(getMeta(e.getItemInHand(), "quantity"));
								cancel();
								gens.remove(p.getUniqueId());
								p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
								new BukkitRunnable() {

									@Override
									public void run() {
										p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);

									}
								}.runTaskLater(CobbleCubes.getInstance(), 6);
								new BukkitRunnable() {

									@Override
									public void run() {
										p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);

									}
								}.runTaskLater(CobbleCubes.getInstance(), 6);
							}
							Random r = new Random();
							final Location newLocation = newLocations.get(p.getUniqueId())
									.get(r.nextInt(newLocations.get(p.getUniqueId()).size()));
							final Material material = materials.get(0);
							type.put(newLocation, "5x5");
							PlaceGeneratorEvent.center.put(newLocation, blockplaced.getLocation().add(0, 3, 0));
							newLocation.getBlock().setType(material);

							materials.remove(0);
							if (CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId()) == null) {
								CobbleCubes.getInstance().allGeneratorBlocks.put(p.getUniqueId(),
										new ArrayList<Location>() {
											{
												add(newLocation);
											}
										});
							} else {
								CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId()).add(newLocation);
							}
							newLocations.get(p.getUniqueId()).remove(newLocation);
						}
					}.runTaskTimer(CobbleCubes.getInstance(), 10, 10).getTaskId();
					gens.put(player.getUniqueId(), task);

					if (e.getItemInHand().getAmount() > 1) {
						player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
					} else {
						player.setItemInHand(new ItemStack(Material.AIR));
					}
				}
			} else {
				Messages.TOCLOSETOPLACE.send(player);
				e.setCancelled(true);
			}
		} else if (e.getItemInHand().getItemMeta().getLore().get(2).equalsIgnoreCase(" �6�l* �e�lSize: �r�l7x7")) {
			e.setCancelled(true);
			final Block blockplaced = e.getBlockPlaced();
			if (player.getLocation().distance(blockplaced.getLocation().add(0.5, 0.0, 0.5)) >= 2.2) {

				blockplaced.getLocation().add(0, 1, 0);
				boolean r2 = true;
				boolean ready = true;
				Label_0336: for (int x = blockplaced.getLocation().getBlockX() - 4; x <= blockplaced.getLocation()
						.getBlockX() + 4; ++x) {
					for (int y = blockplaced.getLocation().getBlockY(); y <= blockplaced.getLocation().getBlockY()
							+ 9; ++y) {
						for (int z = blockplaced.getLocation().getBlockZ() - 4; z <= blockplaced.getLocation()
								.getBlockZ() + 4; ++z) {
							if (blockplaced.getWorld().getBlockAt(x, y, z).getType() != Material.AIR) {
								if (!blockplaced.getLocation().equals((Object) new Location(blockplaced.getWorld(),
										(double) x, (double) y, (double) z))) {
									ready = false;
									Messages.NOTENOUGHSPACE.send(player);
									e.setCancelled(true);
									break Label_0336;
								}
							}
						}
					}
				}
				if (!r2)
					return;
				Label_2591: for (int x = blockplaced.getLocation().getBlockX() - 3; x <= blockplaced.getLocation()
						.getBlockX() + 3; ++x) {
					for (int y = blockplaced.getLocation().add(0, 1, 0).getBlockY(); y <= blockplaced.getLocation()
							.getBlockY() + 7; ++y) {
						for (int z = blockplaced.getLocation().getBlockZ() - 3; z <= blockplaced.getLocation()
								.getBlockZ() + 3; ++z) {
							if (blockplaced.getWorld().getBlockAt(x, y, z).getType() == Material.AIR
									|| blockplaced.getWorld().getBlockAt(x, y, z) == null) {
								newLocations.get(player.getUniqueId())
										.add(new Location(blockplaced.getWorld(), (double) x, (double) y, (double) z));
							} else {
								if (!blockplaced.getLocation().equals((Object) new Location(blockplaced.getWorld(),
										(double) x, (double) y, (double) z))) {
									ready = false;
									Messages.NOTENOUGHSPACE.send(player);
									e.setCancelled(true);
									newLocations.get(player.getUniqueId()).clear();
									break Label_2591;
								}
							}
						}
					}
				}
				if (ready) {
					System.out.print(newLocations.get(e.getPlayer().getUniqueId()).size());
					cBorder(player, blockplaced.getLocation());
					Messages.SUCCESSFULLYPLACED.send(player);
					generating.add(blockplaced.getLocation().clone().add(0, 4, 0));
					List<Material> mats = new ArrayList<Material>();
					Location center = blockplaced.getLocation().clone().add(0, 4, 0);
					data.createGenerator("7x7", blockplaced.getLocation().clone().add(0, 4, 0),
							getMeta(e.getItemInHand(), "quantity"), getMeta(e.getItemInHand(), "loot"), getMeta(e.getItemInHand(), "multiplier"));
					for (String keys : data.getConfig().getConfigurationSection("gens").getKeys(false)) {
						if (data.getConfig().getInt("gens." + keys + ".center.x") == center.getX()
								&& data.getConfig().getInt("gens." + keys + ".center.y") == center.getY()
								&& data.getConfig().getInt("gens." + keys + ".center.z") == center.getZ()) {
							mats = Randomize.get343RandomBlocks(data.getConfig().getInt("gens." + keys + ".loot"));
						}
					}

					final List<Material> materials = mats;
					int task = new BukkitRunnable() {

						Player p = player;

						@SuppressWarnings("serial")
						@Override
						public void run() {
							if (newLocations.get(p.getUniqueId()) == null
									|| newLocations.get(p.getUniqueId()).size() == 0) {
								generating.remove(blockplaced.getLocation().clone().add(0, 4, 0));
								System.out.print(getMeta(e.getItemInHand(), "quantity"));
								cancel();
								gens.remove(p.getUniqueId());
								p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
								new BukkitRunnable() {

									@Override
									public void run() {
										p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);

									}
								}.runTaskLater(CobbleCubes.getInstance(), 6);
								new BukkitRunnable() {

									@Override
									public void run() {
										p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);

									}
								}.runTaskLater(CobbleCubes.getInstance(), 6);
							}
							Random r = new Random();
							System.out.print(newLocations.get(p.getUniqueId()).size());
							final Location newLocation = newLocations.get(p.getUniqueId())
									.get(r.nextInt(newLocations.get(p.getUniqueId()).size()));
							final Material material = materials.get(0);
							type.put(newLocation, "7x7");
							PlaceGeneratorEvent.center.put(newLocation, blockplaced.getLocation().add(0, 4, 0));
							newLocation.getBlock().setType(material);

							materials.remove(0);
							if (CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId()) == null) {
								CobbleCubes.getInstance().allGeneratorBlocks.put(p.getUniqueId(),
										new ArrayList<Location>() {
											{
												add(newLocation);
											}
										});
							} else {
								CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId()).add(newLocation);
								System.out.print(newLocations.get(p.getUniqueId()).size());
							}
							newLocations.get(p.getUniqueId()).remove(newLocation);
						}
					}.runTaskTimer(CobbleCubes.getInstance(), 3, 3).getTaskId();
					gens.put(player.getUniqueId(), task);

					if (e.getItemInHand().getAmount() > 1) {
						player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
					} else {
						player.setItemInHand(new ItemStack(Material.AIR));
					}
				}
			} else {
				Messages.TOCLOSETOPLACE.send(player);
				e.setCancelled(true);
			}
		} else if (e.getItemInHand().getItemMeta().getLore().get(2).equalsIgnoreCase(" �6�l* �e�lSize: �r�l9x9")) {
			e.setCancelled(true);
			final Block blockplaced = e.getBlockPlaced();
			if (player.getLocation().distance(blockplaced.getLocation().add(0.5, 0.0, 0.5)) >= 2.2) {

				blockplaced.getLocation().add(0, 1, 0);
				boolean r2 = true;
				boolean ready = true;
				Label_0336: for (int x = blockplaced.getLocation().getBlockX() - 5; x <= blockplaced.getLocation()
						.getBlockX() + 5; ++x) {
					for (int y = blockplaced.getLocation().getBlockY(); y <= blockplaced.getLocation().getBlockY()
							+ 11; ++y) {
						for (int z = blockplaced.getLocation().getBlockZ() - 5; z <= blockplaced.getLocation()
								.getBlockZ() + 5; ++z) {
							if (blockplaced.getWorld().getBlockAt(x, y, z).getType() != Material.AIR) {
								if (!blockplaced.getLocation().equals((Object) new Location(blockplaced.getWorld(),
										(double) x, (double) y, (double) z))) {
									r2 = false;
									Messages.NOTENOUGHSPACE.send(player);
									e.setCancelled(true);
									break Label_0336;
								}
							}
						}
					}
				}
				if (!r2)
					return;
				Label_2591: for (int x = blockplaced.getLocation().getBlockX() - 4; x <= blockplaced.getLocation()
						.getBlockX() + 4; ++x) {
					for (int y = blockplaced.getLocation().add(0, 1, 0).getBlockY(); y <= blockplaced.getLocation()
							.getBlockY() + 9; ++y) {
						for (int z = blockplaced.getLocation().getBlockZ() - 4; z <= blockplaced.getLocation()
								.getBlockZ() + 4; ++z) {
							if (blockplaced.getWorld().getBlockAt(x, y, z).getType() == Material.AIR
									|| blockplaced.getWorld().getBlockAt(x, y, z) == null) {
								newLocations.get(player.getUniqueId())
										.add(new Location(blockplaced.getWorld(), (double) x, (double) y, (double) z));
							} else {
								if (!blockplaced.getLocation().equals((Object) new Location(blockplaced.getWorld(),
										(double) x, (double) y, (double) z))) {
									ready = false;
									Messages.NOTENOUGHSPACE.send(player);
									e.setCancelled(true);
									newLocations.get(player.getUniqueId()).clear();
									break Label_2591;
								}
							}
						}
					}
				}
				if (ready) {
					System.out.print(newLocations.get(e.getPlayer().getUniqueId()).size());
					dBorder(player, blockplaced.getLocation());
					Messages.SUCCESSFULLYPLACED.send(player);
					generating.add(blockplaced.getLocation().clone().add(0, 5, 0));
					List<Material> mats = new ArrayList<Material>();
					Location center = blockplaced.getLocation().clone().add(0, 5, 0);
					data.createGenerator("9x9", blockplaced.getLocation().clone().add(0, 5, 0),
							getMeta(e.getItemInHand(), "quantity"), getMeta(e.getItemInHand(), "loot"), getMeta(e.getItemInHand(), "multiplier"));
					for (String keys : data.getConfig().getConfigurationSection("gens").getKeys(false)) {
						if (data.getConfig().getInt("gens." + keys + ".center.x") == center.getX()
								&& data.getConfig().getInt("gens." + keys + ".center.y") == center.getY()
								&& data.getConfig().getInt("gens." + keys + ".center.z") == center.getZ()) {
							mats = Randomize.get729RandomBlocks(data.getConfig().getInt("gens." + keys + ".loot"));
						}
					}

					final List<Material> materials = mats;
					int task = new BukkitRunnable() {
						Player p = player;

						@SuppressWarnings("serial")
						@Override
						public void run() {
							if (newLocations.get(p.getUniqueId()) == null
									|| newLocations.get(p.getUniqueId()).size() == 0) {
								generating.remove(blockplaced.getLocation().clone().add(0, 5, 0));
								System.out.print(getMeta(e.getItemInHand(), "quantity"));
								cancel();
								gens.remove(p.getUniqueId());
								p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
								new BukkitRunnable() {

									@Override
									public void run() {
										p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);

									}
								}.runTaskLater(CobbleCubes.getInstance(), 6);
								new BukkitRunnable() {

									@Override
									public void run() {
										p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);

									}
								}.runTaskLater(CobbleCubes.getInstance(), 6);
							}
							Random r = new Random();
							System.out.print(newLocations.get(p.getUniqueId()).size());
							final Location newLocation = newLocations.get(p.getUniqueId())
									.get(r.nextInt(newLocations.get(p.getUniqueId()).size()));
							final Material material = materials.get(0);
							type.put(newLocation, "9x9");
							PlaceGeneratorEvent.center.put(newLocation, blockplaced.getLocation().add(0, 5, 0));
							newLocation.getBlock().setType(material);

							materials.remove(0);
							if (CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId()) == null) {
								CobbleCubes.getInstance().allGeneratorBlocks.put(p.getUniqueId(),
										new ArrayList<Location>() {
											{
												add(newLocation);
											}
										});
							} else {
								CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId()).add(newLocation);
								System.out.print(newLocations.get(p.getUniqueId()).size());
							}
							newLocations.get(p.getUniqueId()).remove(newLocation);
						}
					}.runTaskTimer(CobbleCubes.getInstance(), 2, 2).getTaskId();
					gens.put(player.getUniqueId(), task);

					if (e.getItemInHand().getAmount() > 1) {
						player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
					} else {
						player.setItemInHand(new ItemStack(Material.AIR));
					}
				}
			} else {
				Messages.TOCLOSETOPLACE.send(player);
				e.setCancelled(true);
			}
		} else if (e.getItemInHand().getItemMeta().getLore().get(2).equalsIgnoreCase(" �6�l* �e�lSize: �r�l11x11")) {
			e.setCancelled(true);
			final Block blockplaced = e.getBlockPlaced();
			if (player.getLocation().distance(blockplaced.getLocation().add(0.5, 0.0, 0.5)) >= 2.2) {

				blockplaced.getLocation().add(0, 1, 0);
				boolean r2 = true;
				boolean ready = true;
				Label_0336: for (int x = blockplaced.getLocation().getBlockX() - 6; x <= blockplaced.getLocation()
						.getBlockX() + 6; ++x) {
					for (int y = blockplaced.getLocation().getBlockY(); y <= blockplaced.getLocation().getBlockY()
							+ 13; ++y) {
						for (int z = blockplaced.getLocation().getBlockZ() - 6; z <= blockplaced.getLocation()
								.getBlockZ() + 6; ++z) {
							if (blockplaced.getWorld().getBlockAt(x, y, z).getType() != Material.AIR) {
								if (!blockplaced.getLocation().equals((Object) new Location(blockplaced.getWorld(),
										(double) x, (double) y, (double) z))) {
									r2 = false;
									Messages.NOTENOUGHSPACE.send(player);
									e.setCancelled(true);
									break Label_0336;
								}
							}
						}
					}
				}
				if (!r2)
					return;
				Label_2591: for (int x = blockplaced.getLocation().getBlockX() - 5; x <= blockplaced.getLocation()
						.getBlockX() + 5; ++x) {
					for (int y = blockplaced.getLocation().add(0, 1, 0).getBlockY(); y <= blockplaced.getLocation()
							.getBlockY() + 11; ++y) {
						for (int z = blockplaced.getLocation().getBlockZ() - 5; z <= blockplaced.getLocation()
								.getBlockZ() + 5; ++z) {
							if (blockplaced.getWorld().getBlockAt(x, y, z).getType() == Material.AIR
									|| blockplaced.getWorld().getBlockAt(x, y, z) == null) {
								newLocations.get(player.getUniqueId())
										.add(new Location(blockplaced.getWorld(), (double) x, (double) y, (double) z));
							} else {
								if (!blockplaced.getLocation().equals((Object) new Location(blockplaced.getWorld(),
										(double) x, (double) y, (double) z))) {
									ready = false;
									Messages.NOTENOUGHSPACE.send(player);
									e.setCancelled(true);
									newLocations.get(player.getUniqueId()).clear();
									break Label_2591;
								}
							}
						}
					}
				}
				if (ready) {
					System.out.print(newLocations.get(e.getPlayer().getUniqueId()).size());
					eBorder(player, blockplaced.getLocation());
					Messages.SUCCESSFULLYPLACED.send(player);
					generating.add(blockplaced.getLocation().clone().add(0, 6, 0));
					List<Material> mats = new ArrayList<Material>();
					Location center = blockplaced.getLocation().clone().add(0, 6, 0);
					data.createGenerator("11x11", blockplaced.getLocation().clone().add(0, 6, 0),
							getMeta(e.getItemInHand(), "quantity"), getMeta(e.getItemInHand(), "loot"), getMeta(e.getItemInHand(), "multiplier"));
					for (String keys : data.getConfig().getConfigurationSection("gens").getKeys(false)) {
						if (data.getConfig().getInt("gens." + keys + ".center.x") == center.getX()
								&& data.getConfig().getInt("gens." + keys + ".center.y") == center.getY()
								&& data.getConfig().getInt("gens." + keys + ".center.z") == center.getZ()) {
							mats = Randomize.get1331RandomBlocks(data.getConfig().getInt("gens." + keys + ".loot"));
						}
					}

					final List<Material> materials = mats;
					int task = new BukkitRunnable() {
						Player p = player;

						@SuppressWarnings("serial")
						@Override
						public void run() {
							if (newLocations.get(p.getUniqueId()) == null
									|| newLocations.get(p.getUniqueId()).size() == 0) {
								generating.remove(blockplaced.getLocation().clone().add(0, 6, 0));
								System.out.print(getMeta(e.getItemInHand(), "quantity"));
								cancel();
								gens.remove(p.getUniqueId());
								p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
								new BukkitRunnable() {

									@Override
									public void run() {
										p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);

									}
								}.runTaskLater(CobbleCubes.getInstance(), 6);
								new BukkitRunnable() {

									@Override
									public void run() {
										p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);

									}
								}.runTaskLater(CobbleCubes.getInstance(), 6);
							}
							Random r = new Random();
							System.out.print(newLocations.get(p.getUniqueId()).size());
							final Location newLocation = newLocations.get(p.getUniqueId())
									.get(r.nextInt(newLocations.get(p.getUniqueId()).size()));
							final Material material = materials.get(0);
							type.put(newLocation, "11x11");
							PlaceGeneratorEvent.center.put(newLocation, blockplaced.getLocation().add(0, 6, 0));
							newLocation.getBlock().setType(material);

							materials.remove(0);
							if (CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId()) == null) {
								CobbleCubes.getInstance().allGeneratorBlocks.put(p.getUniqueId(),
										new ArrayList<Location>() {
											{
												add(newLocation);
											}
										});
							} else {
								CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId()).add(newLocation);
								System.out.print(newLocations.get(p.getUniqueId()).size());
							}
							newLocations.get(p.getUniqueId()).remove(newLocation);
						}
					}.runTaskTimer(CobbleCubes.getInstance(), 1, 1).getTaskId();
					gens.put(player.getUniqueId(), task);

					if (e.getItemInHand().getAmount() > 1) {
						player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
					} else {
						player.setItemInHand(new ItemStack(Material.AIR));
					}
				}
			} else {
				Messages.TOCLOSETOPLACE.send(player);
				e.setCancelled(true);
			}
		} else if (e.getItemInHand().getItemMeta().getLore().get(2).equalsIgnoreCase(" �6�l* �e�lSize: �r�l13x13")) {
			e.setCancelled(true);
			final Block blockplaced = e.getBlockPlaced();
			if (player.getLocation().distance(blockplaced.getLocation().add(0.5, 0.0, 0.5)) >= 2.2) {

				blockplaced.getLocation().add(0, 1, 0);
				boolean r2 = true;
				boolean ready = true;
				Label_0336: for (int x = blockplaced.getLocation().getBlockX() - 7; x <= blockplaced.getLocation()
						.getBlockX() + 7; ++x) {
					for (int y = blockplaced.getLocation().getBlockY(); y <= blockplaced.getLocation().getBlockY()
							+ 15; ++y) {
						for (int z = blockplaced.getLocation().getBlockZ() - 7; z <= blockplaced.getLocation()
								.getBlockZ() + 7; ++z) {
							if (blockplaced.getWorld().getBlockAt(x, y, z).getType() != Material.AIR) {
								if (!blockplaced.getLocation().equals((Object) new Location(blockplaced.getWorld(),
										(double) x, (double) y, (double) z))) {
									r2 = false;
									Messages.NOTENOUGHSPACE.send(player);
									e.setCancelled(true);
									break Label_0336;
								}
							}
						}
					}
				}
				if (!r2)
					return;
				Label_2591: for (int x = blockplaced.getLocation().getBlockX() - 6; x <= blockplaced.getLocation()
						.getBlockX() + 6; ++x) {
					for (int y = blockplaced.getLocation().add(0, 1, 0).getBlockY(); y <= blockplaced.getLocation()
							.getBlockY() + 13; ++y) {
						for (int z = blockplaced.getLocation().getBlockZ() - 6; z <= blockplaced.getLocation()
								.getBlockZ() + 6; ++z) {
							if (blockplaced.getWorld().getBlockAt(x, y, z).getType() == Material.AIR
									|| blockplaced.getWorld().getBlockAt(x, y, z) == null) {
								newLocations.get(player.getUniqueId())
										.add(new Location(blockplaced.getWorld(), (double) x, (double) y, (double) z));
							} else {
								if (!blockplaced.getLocation().equals((Object) new Location(blockplaced.getWorld(),
										(double) x, (double) y, (double) z))) {
									ready = false;
									Messages.NOTENOUGHSPACE.send(player);
									e.setCancelled(true);
									newLocations.get(player.getUniqueId()).clear();
									break Label_2591;
								}
							}
						}
					}
				}
				if (ready) {
					System.out.print(newLocations.get(e.getPlayer().getUniqueId()).size());
					fBorder(player, blockplaced.getLocation());
					Messages.SUCCESSFULLYPLACED.send(player);
					generating.add(blockplaced.getLocation().clone().add(0, 7, 0));
					List<Material> mats = new ArrayList<Material>();
					Location center = blockplaced.getLocation().clone().add(0, 7, 0);
					data.createGenerator("13x13", blockplaced.getLocation().clone().add(0, 7, 0),
							getMeta(e.getItemInHand(), "quantity"), getMeta(e.getItemInHand(), "loot"), getMeta(e.getItemInHand(), "multiplier"));
					for (String keys : data.getConfig().getConfigurationSection("gens").getKeys(false)) {
						if (data.getConfig().getInt("gens." + keys + ".center.x") == center.getX()
								&& data.getConfig().getInt("gens." + keys + ".center.y") == center.getY()
								&& data.getConfig().getInt("gens." + keys + ".center.z") == center.getZ()) {
							mats = Randomize.get2197RandomBlocks(data.getConfig().getInt("gens." + keys + ".loot"));
						}
					}

					final List<Material> materials = mats;
					int task = new BukkitRunnable() {
						Player p = player;

						@SuppressWarnings("serial")
						@Override
						public void run() {
							if (newLocations.get(p.getUniqueId()) == null
									|| newLocations.get(p.getUniqueId()).size() == 0) {
								generating.remove(blockplaced.getLocation().clone().add(0, 7, 0));
								System.out.print(getMeta(e.getItemInHand(), "quantity"));
								cancel();
								gens.remove(p.getUniqueId());
								p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
								new BukkitRunnable() {

									@Override
									public void run() {
										p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);

									}
								}.runTaskLater(CobbleCubes.getInstance(), 6);
								new BukkitRunnable() {

									@Override
									public void run() {
										p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);

									}
								}.runTaskLater(CobbleCubes.getInstance(), 6);
							}
							Random r = new Random();
							System.out.print(newLocations.get(p.getUniqueId()).size());
							final Location newLocation = newLocations.get(p.getUniqueId())
									.get(r.nextInt(newLocations.get(p.getUniqueId()).size()));
							final Material material = materials.get(0);
							type.put(newLocation, "13x13");
							PlaceGeneratorEvent.center.put(newLocation, blockplaced.getLocation().add(0, 7, 0));
							newLocation.getBlock().setType(material);

							materials.remove(0);
							if (CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId()) == null) {
								CobbleCubes.getInstance().allGeneratorBlocks.put(p.getUniqueId(),
										new ArrayList<Location>() {
											{
												add(newLocation);
											}
										});
							} else {
								CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId()).add(newLocation);
								System.out.print(newLocations.get(p.getUniqueId()).size());
							}
							newLocations.get(p.getUniqueId()).remove(newLocation);
						}
					}.runTaskTimer(CobbleCubes.getInstance(), 1, 1).getTaskId();
					gens.put(player.getUniqueId(), task);

					if (e.getItemInHand().getAmount() > 1) {
						player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
					} else {
						player.setItemInHand(new ItemStack(Material.AIR));
					}
				}
			} else {
				Messages.TOCLOSETOPLACE.send(player);
				e.setCancelled(true);
			}
		}
	}

	@SuppressWarnings("serial")
	public void aBorder(Player p, Location cl) {
		ArrayList<Location> b = new ArrayList<Location>();

		cl.add(0, 2, 0);

		// bF Edge
		b.add(cl.clone().add(0, -2, 2));
		b.add(cl.clone().add(1, -2, 2));
		b.add(cl.clone().add(-1, -2, 2));

		// bB Edge
		b.add(cl.clone().add(0, -2, -2));
		b.add(cl.clone().add(1, -2, -2));
		b.add(cl.clone().add(-1, -2, -2));

		// bL Edge
		b.add(cl.clone().add(-2, -2, -1));
		b.add(cl.clone().add(-2, -2, 0));
		b.add(cl.clone().add(-2, -2, 1));

		// bR Edge
		b.add(cl.clone().add(2, -2, -1));
		b.add(cl.clone().add(2, -2, 0));
		b.add(cl.clone().add(2, -2, 1));

		// tF Edge
		b.add(cl.clone().add(0, 2, 2));
		b.add(cl.clone().add(1, 2, 2));
		b.add(cl.clone().add(-1, 2, 2));

		// tB Edge
		b.add(cl.clone().add(0, 2, -2));
		b.add(cl.clone().add(1, 2, -2));
		b.add(cl.clone().add(-1, 2, -2));

		// tL Edge
		b.add(cl.clone().add(-2, 2, -1));
		b.add(cl.clone().add(-2, 2, 0));
		b.add(cl.clone().add(-2, 2, 1));

		// tR Edge
		b.add(cl.clone().add(2, 2, -1));
		b.add(cl.clone().add(2, 2, 0));
		b.add(cl.clone().add(2, 2, 1));

		// fL Edge
		b.add(cl.clone().add(2, 0, -2));
		b.add(cl.clone().add(2, 1, -2));
		b.add(cl.clone().add(2, -1, -2));

		// fR Edge
		b.add(cl.clone().add(2, 0, 2));
		b.add(cl.clone().add(2, 1, 2));
		b.add(cl.clone().add(2, -1, 2));

		// bL Edge
		b.add(cl.clone().add(-2, 0, -2));
		b.add(cl.clone().add(-2, 1, -2));
		b.add(cl.clone().add(-2, -1, -2));

		// bR Edge
		b.add(cl.clone().add(-2, 0, 2));
		b.add(cl.clone().add(-2, 1, 2));
		b.add(cl.clone().add(-2, -1, 2));

		// Bottom Corners
		b.add(cl.clone().add(2, -2, 2));
		b.add(cl.clone().add(-2, -2, 2));
		b.add(cl.clone().add(-2, -2, -2));
		b.add(cl.clone().add(2, -2, -2));

		// Top Corners
		b.add(cl.clone().add(2, 2, 2));
		b.add(cl.clone().add(-2, 2, 2));
		b.add(cl.clone().add(-2, 2, -2));
		b.add(cl.clone().add(2, 2, -2));

		for (final Location l : b) {
			center.put(l, cl);
			if (CobbleCubes.getInstance().allBorderBlocks.get(p.getUniqueId()) == null) {
				CobbleCubes.getInstance().allBorderBlocks.put(p.getUniqueId(), new ArrayList<Location>() {
					{
						add(l);
					}
				});
			} else {
				CobbleCubes.getInstance().allBorderBlocks.get(p.getUniqueId()).add(l);
			}
			new BukkitRunnable() {
				@Override
				public void run() {
					l.getBlock().setType(Material.BEDROCK);
				}
			}.runTaskLater(CobbleCubes.getInstance(), 5);
		}
	}

	@SuppressWarnings("serial")
	public void bBorder(Player p, Location cl) {
		ArrayList<Location> b = new ArrayList<Location>();

		cl.add(0, 2, 0);

		// bF Edge
		b.add(cl.clone().add(0, -2, 3));
		b.add(cl.clone().add(1, -2, 3));
		b.add(cl.clone().add(-1, -2, 3));
		b.add(cl.clone().add(2, -2, 3));
		b.add(cl.clone().add(-2, -2, 3));

		// bB Edge
		b.add(cl.clone().add(0, -2, -3));
		b.add(cl.clone().add(1, -2, -3));
		b.add(cl.clone().add(-1, -2, -3));
		b.add(cl.clone().add(2, -2, -3));
		b.add(cl.clone().add(-2, -2, -3));

		// bL Edge
		b.add(cl.clone().add(-3, -2, -1));
		b.add(cl.clone().add(-3, -2, 0));
		b.add(cl.clone().add(-3, -2, 1));
		b.add(cl.clone().add(-3, -2, -2));
		b.add(cl.clone().add(-3, -2, 2));

		// bR Edge
		b.add(cl.clone().add(3, -2, -1));
		b.add(cl.clone().add(3, -2, 0));
		b.add(cl.clone().add(3, -2, 1));
		b.add(cl.clone().add(3, -2, -2));
		b.add(cl.clone().add(3, -2, 2));

		// tF Edge
		b.add(cl.clone().add(0, 4, 3));
		b.add(cl.clone().add(1, 4, 3));
		b.add(cl.clone().add(-1, 4, 3));
		b.add(cl.clone().add(2, 4, 3));
		b.add(cl.clone().add(-2, 4, 3));

		// tB Edge
		b.add(cl.clone().add(0, 4, -3));
		b.add(cl.clone().add(1, 4, -3));
		b.add(cl.clone().add(-1, 4, -3));
		b.add(cl.clone().add(2, 4, -3));
		b.add(cl.clone().add(-2, 4, -3));

		// tL Edge
		b.add(cl.clone().add(-3, 4, -1));
		b.add(cl.clone().add(-3, 4, 0));
		b.add(cl.clone().add(-3, 4, 1));
		b.add(cl.clone().add(-3, 4, -2));
		b.add(cl.clone().add(-3, 4, 2));

		// tR Edge
		b.add(cl.clone().add(3, 4, -1));
		b.add(cl.clone().add(3, 4, 0));
		b.add(cl.clone().add(3, 4, 1));
		b.add(cl.clone().add(3, 4, -2));
		b.add(cl.clone().add(3, 4, 2));

		// fL Edge
		b.add(cl.clone().add(3, 3, -3));
		b.add(cl.clone().add(3, 2, -3));
		b.add(cl.clone().add(3, 0, -3));
		b.add(cl.clone().add(3, 1, -3));
		b.add(cl.clone().add(3, -1, -3));

		// fR Edge
		b.add(cl.clone().add(3, 3, 3));
		b.add(cl.clone().add(3, 2, 3));
		b.add(cl.clone().add(3, 0, 3));
		b.add(cl.clone().add(3, 1, 3));
		b.add(cl.clone().add(3, -1, 3));

		// bL Edge
		b.add(cl.clone().add(-3, 3, -3));
		b.add(cl.clone().add(-3, 2, -3));
		b.add(cl.clone().add(-3, 0, -3));
		b.add(cl.clone().add(-3, 1, -3));
		b.add(cl.clone().add(-3, -1, -3));

		// bR Edge
		b.add(cl.clone().add(-3, 3, 3));
		b.add(cl.clone().add(-3, 2, 3));
		b.add(cl.clone().add(-3, 0, 3));
		b.add(cl.clone().add(-3, 1, 3));
		b.add(cl.clone().add(-3, -1, 3));

		// Bottom Corners
		b.add(cl.clone().add(3, -2, 3));
		b.add(cl.clone().add(-3, -2, 3));
		b.add(cl.clone().add(-3, -2, -3));
		b.add(cl.clone().add(3, -2, -3));

		// Top Corners
		b.add(cl.clone().add(3, 4, 3));
		b.add(cl.clone().add(-3, 4, 3));
		b.add(cl.clone().add(-3, 4, -3));
		b.add(cl.clone().add(3, 4, -3));

		for (final Location l : b) {
			center.put(l, cl);
			if (CobbleCubes.getInstance().allBorderBlocks.get(p.getUniqueId()) == null) {
				CobbleCubes.getInstance().allBorderBlocks.put(p.getUniqueId(), new ArrayList<Location>() {
					{
						add(l);
					}
				});
			} else {
				CobbleCubes.getInstance().allBorderBlocks.get(p.getUniqueId()).add(l);
			}
			new BukkitRunnable() {
				@Override
				public void run() {
					l.getBlock().setType(Material.BEDROCK);
				}
			}.runTaskLater(CobbleCubes.getInstance(), 5);
		}
	}

	@SuppressWarnings("serial")
	public void cBorder(Player p, Location cl) {
		ArrayList<Location> b = new ArrayList<Location>();

		cl.add(0, 2, 0);

		// bF Edge
		b.add(cl.clone().add(0, -2, 4));
		b.add(cl.clone().add(1, -2, 4));
		b.add(cl.clone().add(-1, -2, 4));
		b.add(cl.clone().add(2, -2, 4));
		b.add(cl.clone().add(-2, -2, 4));
		b.add(cl.clone().add(3, -2, 4));
		b.add(cl.clone().add(-3, -2, 4));

		// bB Edge
		b.add(cl.clone().add(0, -2, -4));
		b.add(cl.clone().add(1, -2, -4));
		b.add(cl.clone().add(-1, -2, -4));
		b.add(cl.clone().add(2, -2, -4));
		b.add(cl.clone().add(-2, -2, -4));
		b.add(cl.clone().add(3, -2, -4));
		b.add(cl.clone().add(-3, -2, -4));

		// bL Edge
		b.add(cl.clone().add(-4, -2, -1));
		b.add(cl.clone().add(-4, -2, 0));
		b.add(cl.clone().add(-4, -2, 1));
		b.add(cl.clone().add(-4, -2, -2));
		b.add(cl.clone().add(-4, -2, 2));
		b.add(cl.clone().add(-4, -2, -3));
		b.add(cl.clone().add(-4, -2, 3));

		// bR Edge
		b.add(cl.clone().add(4, -2, -1));
		b.add(cl.clone().add(4, -2, 0));
		b.add(cl.clone().add(4, -2, 1));
		b.add(cl.clone().add(4, -2, -2));
		b.add(cl.clone().add(4, -2, 2));
		b.add(cl.clone().add(4, -2, -3));
		b.add(cl.clone().add(4, -2, 3));

		// tF Edge
		b.add(cl.clone().add(0, 6, 4));
		b.add(cl.clone().add(1, 6, 4));
		b.add(cl.clone().add(-1, 6, 4));
		b.add(cl.clone().add(2, 6, 4));
		b.add(cl.clone().add(-2, 6, 4));
		b.add(cl.clone().add(3, 6, 4));
		b.add(cl.clone().add(-3, 6, 4));

		// tB Edge
		b.add(cl.clone().add(0, 6, -4));
		b.add(cl.clone().add(1, 6, -4));
		b.add(cl.clone().add(-1, 6, -4));
		b.add(cl.clone().add(2, 6, -4));
		b.add(cl.clone().add(-2, 6, -4));
		b.add(cl.clone().add(3, 6, -4));
		b.add(cl.clone().add(-3, 6, -4));

		// tL Edge
		b.add(cl.clone().add(-4, 6, -1));
		b.add(cl.clone().add(-4, 6, 0));
		b.add(cl.clone().add(-4, 6, 1));
		b.add(cl.clone().add(-4, 6, -2));
		b.add(cl.clone().add(-4, 6, 2));
		b.add(cl.clone().add(-4, 6, 3));
		b.add(cl.clone().add(-4, 6, -3));

		// tR Edge
		b.add(cl.clone().add(4, 6, -1));
		b.add(cl.clone().add(4, 6, 0));
		b.add(cl.clone().add(4, 6, 1));
		b.add(cl.clone().add(4, 6, -2));
		b.add(cl.clone().add(4, 6, 2));
		b.add(cl.clone().add(4, 6, -3));
		b.add(cl.clone().add(4, 6, 3));

		// fL Edge
		b.add(cl.clone().add(4, 3, -4));
		b.add(cl.clone().add(4, 2, -4));
		b.add(cl.clone().add(4, 0, -4));
		b.add(cl.clone().add(4, 1, -4));
		b.add(cl.clone().add(4, -1, -4));
		b.add(cl.clone().add(4, 4, -4));
		b.add(cl.clone().add(4, 5, -4));

		// fR Edge
		b.add(cl.clone().add(4, 3, 4));
		b.add(cl.clone().add(4, 2, 4));
		b.add(cl.clone().add(4, 0, 4));
		b.add(cl.clone().add(4, 1, 4));
		b.add(cl.clone().add(4, -1, 4));
		b.add(cl.clone().add(4, 4, 4));
		b.add(cl.clone().add(4, 5, 4));

		// bL Edge
		b.add(cl.clone().add(-4, 3, -4));
		b.add(cl.clone().add(-4, 2, -4));
		b.add(cl.clone().add(-4, 0, -4));
		b.add(cl.clone().add(-4, 1, -4));
		b.add(cl.clone().add(-4, -1, -4));
		b.add(cl.clone().add(-4, 4, -4));
		b.add(cl.clone().add(-4, 5, -4));

		// bR Edge
		b.add(cl.clone().add(-4, 3, 4));
		b.add(cl.clone().add(-4, 2, 4));
		b.add(cl.clone().add(-4, 0, 4));
		b.add(cl.clone().add(-4, 1, 4));
		b.add(cl.clone().add(-4, -1, 4));
		b.add(cl.clone().add(-4, 4, 4));
		b.add(cl.clone().add(-4, 5, 4));

		// Bottom Corners
		b.add(cl.clone().add(4, -2, 4));
		b.add(cl.clone().add(-4, -2, 4));
		b.add(cl.clone().add(-4, -2, -4));
		b.add(cl.clone().add(4, -2, -4));

		// Top Corners
		b.add(cl.clone().add(4, 6, 4));
		b.add(cl.clone().add(-4, 6, 4));
		b.add(cl.clone().add(-4, 6, -4));
		b.add(cl.clone().add(4, 6, -4));

		for (final Location l : b) {
			center.put(l, cl);
			if (CobbleCubes.getInstance().allBorderBlocks.get(p.getUniqueId()) == null) {
				CobbleCubes.getInstance().allBorderBlocks.put(p.getUniqueId(), new ArrayList<Location>() {
					{
						add(l);
					}
				});
			} else {
				CobbleCubes.getInstance().allBorderBlocks.get(p.getUniqueId()).add(l);
			}
			new BukkitRunnable() {
				@Override
				public void run() {
					l.getBlock().setType(Material.BEDROCK);
				}
			}.runTaskLater(CobbleCubes.getInstance(), 5);
		}
	}

	@SuppressWarnings("serial")
	public void dBorder(Player p, Location cl) {
		ArrayList<Location> b = new ArrayList<Location>();

		cl.add(0, 2, 0);

		// bF Edge
		b.add(cl.clone().add(0, -2, 5));
		b.add(cl.clone().add(1, -2, 5));
		b.add(cl.clone().add(-1, -2, 5));
		b.add(cl.clone().add(2, -2, 5));
		b.add(cl.clone().add(-2, -2, 5));
		b.add(cl.clone().add(3, -2, 5));
		b.add(cl.clone().add(-3, -2, 5));
		b.add(cl.clone().add(4, -2, 5));
		b.add(cl.clone().add(-4, -2, 5));

		// bB Edge
		b.add(cl.clone().add(0, -2, -5));
		b.add(cl.clone().add(1, -2, -5));
		b.add(cl.clone().add(-1, -2, -5));
		b.add(cl.clone().add(2, -2, -5));
		b.add(cl.clone().add(-2, -2, -5));
		b.add(cl.clone().add(3, -2, -5));
		b.add(cl.clone().add(-3, -2, -5));
		b.add(cl.clone().add(4, -2, -5));
		b.add(cl.clone().add(-4, -2, -5));

		// bL Edge
		b.add(cl.clone().add(-5, -2, -1));
		b.add(cl.clone().add(-5, -2, 0));
		b.add(cl.clone().add(-5, -2, 1));
		b.add(cl.clone().add(-5, -2, -2));
		b.add(cl.clone().add(-5, -2, 2));
		b.add(cl.clone().add(-5, -2, -3));
		b.add(cl.clone().add(-5, -2, 3));
		b.add(cl.clone().add(-5, -2, -4));
		b.add(cl.clone().add(-5, -2, 4));

		// bR Edge
		b.add(cl.clone().add(5, -2, -1));
		b.add(cl.clone().add(5, -2, 0));
		b.add(cl.clone().add(5, -2, 1));
		b.add(cl.clone().add(5, -2, -2));
		b.add(cl.clone().add(5, -2, 2));
		b.add(cl.clone().add(5, -2, -3));
		b.add(cl.clone().add(5, -2, 3));
		b.add(cl.clone().add(5, -2, -4));
		b.add(cl.clone().add(5, -2, 4));

		// tF Edge
		b.add(cl.clone().add(0, 8, 5));
		b.add(cl.clone().add(1, 8, 5));
		b.add(cl.clone().add(-1, 8, 5));
		b.add(cl.clone().add(2, 8, 5));
		b.add(cl.clone().add(-2, 8, 5));
		b.add(cl.clone().add(3, 8, 5));
		b.add(cl.clone().add(-3, 8, 5));
		b.add(cl.clone().add(4, 8, 5));
		b.add(cl.clone().add(-4, 8, 5));

		// tB Edge
		b.add(cl.clone().add(0, 8, -5));
		b.add(cl.clone().add(1, 8, -5));
		b.add(cl.clone().add(-1, 8, -5));
		b.add(cl.clone().add(2, 8, -5));
		b.add(cl.clone().add(-2, 8, -5));
		b.add(cl.clone().add(3, 8, -5));
		b.add(cl.clone().add(-3, 8, -5));
		b.add(cl.clone().add(4, 8, -5));
		b.add(cl.clone().add(-4, 8, -5));

		// tL Edge
		b.add(cl.clone().add(-5, 8, -1));
		b.add(cl.clone().add(-5, 8, 0));
		b.add(cl.clone().add(-5, 8, 1));
		b.add(cl.clone().add(-5, 8, -2));
		b.add(cl.clone().add(-5, 8, 2));
		b.add(cl.clone().add(-5, 8, 3));
		b.add(cl.clone().add(-5, 8, -3));
		b.add(cl.clone().add(-5, 8, 4));
		b.add(cl.clone().add(-5, 8, -4));

		// tR Edge
		b.add(cl.clone().add(5, 8, -1));
		b.add(cl.clone().add(5, 8, 0));
		b.add(cl.clone().add(5, 8, 1));
		b.add(cl.clone().add(5, 8, -2));
		b.add(cl.clone().add(5, 8, 2));
		b.add(cl.clone().add(5, 8, -3));
		b.add(cl.clone().add(5, 8, 3));
		b.add(cl.clone().add(5, 8, -3));
		b.add(cl.clone().add(5, 8, 4));
		b.add(cl.clone().add(5, 8, -4));

		// fL Edge
		b.add(cl.clone().add(5, 3, -5));
		b.add(cl.clone().add(5, 2, -5));
		b.add(cl.clone().add(5, 0, -5));
		b.add(cl.clone().add(5, 1, -5));
		b.add(cl.clone().add(5, -1, -5));
		b.add(cl.clone().add(5, 4, -5));
		b.add(cl.clone().add(5, 5, -5));
		b.add(cl.clone().add(5, 6, -5));
		b.add(cl.clone().add(5, 7, -5));

		// fR Edge
		b.add(cl.clone().add(5, 3, 5));
		b.add(cl.clone().add(5, 2, 5));
		b.add(cl.clone().add(5, 0, 5));
		b.add(cl.clone().add(5, 1, 5));
		b.add(cl.clone().add(5, -1, 5));
		b.add(cl.clone().add(5, 4, 5));
		b.add(cl.clone().add(5, 5, 5));
		b.add(cl.clone().add(5, 6, 5));
		b.add(cl.clone().add(5, 7, 5));

		// bL Edge
		b.add(cl.clone().add(-5, 3, -5));
		b.add(cl.clone().add(-5, 2, -5));
		b.add(cl.clone().add(-5, 0, -5));
		b.add(cl.clone().add(-5, 1, -5));
		b.add(cl.clone().add(-5, -1, -5));
		b.add(cl.clone().add(-5, 4, -5));
		b.add(cl.clone().add(-5, 5, -5));
		b.add(cl.clone().add(-5, 6, -5));
		b.add(cl.clone().add(-5, 7, -5));

		// bR Edge
		b.add(cl.clone().add(-5, 3, 5));
		b.add(cl.clone().add(-5, 2, 5));
		b.add(cl.clone().add(-5, 0, 5));
		b.add(cl.clone().add(-5, 1, 5));
		b.add(cl.clone().add(-5, -1, 5));
		b.add(cl.clone().add(-5, 4, 5));
		b.add(cl.clone().add(-5, 5, 5));
		b.add(cl.clone().add(-5, 6, 5));
		b.add(cl.clone().add(-5, 7, 5));

		// Bottom Corners
		b.add(cl.clone().add(5, -2, 5));
		b.add(cl.clone().add(-5, -2, 5));
		b.add(cl.clone().add(-5, -2, -5));
		b.add(cl.clone().add(5, -2, -5));

		// Top Corners
		b.add(cl.clone().add(5, 8, 5));
		b.add(cl.clone().add(-5, 8, 5));
		b.add(cl.clone().add(-5, 8, -5));
		b.add(cl.clone().add(5, 8, -5));

		for (final Location l : b) {
			center.put(l, cl);
			if (CobbleCubes.getInstance().allBorderBlocks.get(p.getUniqueId()) == null) {
				CobbleCubes.getInstance().allBorderBlocks.put(p.getUniqueId(), new ArrayList<Location>() {
					{
						add(l);
					}
				});
			} else {
				CobbleCubes.getInstance().allBorderBlocks.get(p.getUniqueId()).add(l);
			}
			new BukkitRunnable() {
				@Override
				public void run() {
					l.getBlock().setType(Material.BEDROCK);
				}
			}.runTaskLater(CobbleCubes.getInstance(), 5);
		}
	}

	@SuppressWarnings("serial")
	public void eBorder(Player p, Location cl) {
		ArrayList<Location> b = new ArrayList<Location>();

		cl.add(0, 2, 0);

		// bF Edge
		b.add(cl.clone().add(0, -2, 6));
		b.add(cl.clone().add(1, -2, 6));
		b.add(cl.clone().add(-1, -2, 6));
		b.add(cl.clone().add(2, -2, 6));
		b.add(cl.clone().add(-2, -2, 6));
		b.add(cl.clone().add(3, -2, 6));
		b.add(cl.clone().add(-3, -2, 6));
		b.add(cl.clone().add(4, -2, 6));
		b.add(cl.clone().add(-4, -2, 6));
		b.add(cl.clone().add(5, -2, 6));
		b.add(cl.clone().add(-5, -2, 6));

		// bB Edge
		b.add(cl.clone().add(0, -2, -6));
		b.add(cl.clone().add(1, -2, -6));
		b.add(cl.clone().add(-1, -2, -6));
		b.add(cl.clone().add(2, -2, -6));
		b.add(cl.clone().add(-2, -2, -6));
		b.add(cl.clone().add(3, -2, -6));
		b.add(cl.clone().add(-3, -2, -6));
		b.add(cl.clone().add(4, -2, -6));
		b.add(cl.clone().add(-4, -2, -6));
		b.add(cl.clone().add(5, -2, -6));
		b.add(cl.clone().add(-5, -2, -6));

		// bL Edge
		b.add(cl.clone().add(-6, -2, -1));
		b.add(cl.clone().add(-6, -2, 0));
		b.add(cl.clone().add(-6, -2, 1));
		b.add(cl.clone().add(-6, -2, -2));
		b.add(cl.clone().add(-6, -2, 2));
		b.add(cl.clone().add(-6, -2, -3));
		b.add(cl.clone().add(-6, -2, 3));
		b.add(cl.clone().add(-6, -2, -4));
		b.add(cl.clone().add(-6, -2, 4));
		b.add(cl.clone().add(-6, -2, -5));
		b.add(cl.clone().add(-6, -2, 5));

		// bR Edge
		b.add(cl.clone().add(6, -2, -1));
		b.add(cl.clone().add(6, -2, 0));
		b.add(cl.clone().add(6, -2, 1));
		b.add(cl.clone().add(6, -2, -2));
		b.add(cl.clone().add(6, -2, 2));
		b.add(cl.clone().add(6, -2, -3));
		b.add(cl.clone().add(6, -2, 3));
		b.add(cl.clone().add(6, -2, -4));
		b.add(cl.clone().add(6, -2, 4));
		b.add(cl.clone().add(6, -2, -5));
		b.add(cl.clone().add(6, -2, 5));

		// tF Edge
		b.add(cl.clone().add(0, 10, 6));
		b.add(cl.clone().add(1, 10, 6));
		b.add(cl.clone().add(-1, 10, 6));
		b.add(cl.clone().add(2, 10, 6));
		b.add(cl.clone().add(-2, 10, 6));
		b.add(cl.clone().add(3, 10, 6));
		b.add(cl.clone().add(-3, 10, 6));
		b.add(cl.clone().add(4, 10, 6));
		b.add(cl.clone().add(-4, 10, 6));
		b.add(cl.clone().add(5, 10, 6));
		b.add(cl.clone().add(-5, 10, 6));

		// tB Edge
		b.add(cl.clone().add(0, 10, -6));
		b.add(cl.clone().add(1, 10, -6));
		b.add(cl.clone().add(-1, 10, -6));
		b.add(cl.clone().add(2, 10, -6));
		b.add(cl.clone().add(-2, 10, -6));
		b.add(cl.clone().add(3, 10, -6));
		b.add(cl.clone().add(-3, 10, -6));
		b.add(cl.clone().add(4, 10, -6));
		b.add(cl.clone().add(-4, 10, -6));
		b.add(cl.clone().add(5, 10, -6));
		b.add(cl.clone().add(-5, 10, -6));

		// tL Edge
		b.add(cl.clone().add(-6, 10, -1));
		b.add(cl.clone().add(-6, 10, 0));
		b.add(cl.clone().add(-6, 10, 1));
		b.add(cl.clone().add(-6, 10, -2));
		b.add(cl.clone().add(-6, 10, 2));
		b.add(cl.clone().add(-6, 10, 3));
		b.add(cl.clone().add(-6, 10, -3));
		b.add(cl.clone().add(-6, 10, 4));
		b.add(cl.clone().add(-6, 10, -4));
		b.add(cl.clone().add(-6, 10, 5));
		b.add(cl.clone().add(-6, 10, -5));

		// tR Edge
		b.add(cl.clone().add(6, 10, -1));
		b.add(cl.clone().add(6, 10, 0));
		b.add(cl.clone().add(6, 10, 1));
		b.add(cl.clone().add(6, 10, -2));
		b.add(cl.clone().add(6, 10, 2));
		b.add(cl.clone().add(6, 10, -3));
		b.add(cl.clone().add(6, 10, 3));
		b.add(cl.clone().add(6, 10, -3));
		b.add(cl.clone().add(6, 10, 4));
		b.add(cl.clone().add(6, 10, -4));
		b.add(cl.clone().add(6, 10, 5));
		b.add(cl.clone().add(6, 10, -5));

		// fL Edge
		b.add(cl.clone().add(6, 3, -6));
		b.add(cl.clone().add(6, 2, -6));
		b.add(cl.clone().add(6, 0, -6));
		b.add(cl.clone().add(6, 1, -6));
		b.add(cl.clone().add(6, -1, -6));
		b.add(cl.clone().add(6, 4, -6));
		b.add(cl.clone().add(6, 5, -6));
		b.add(cl.clone().add(6, 6, -6));
		b.add(cl.clone().add(6, 7, -6));
		b.add(cl.clone().add(6, 8, -6));
		b.add(cl.clone().add(6, 9, -6));

		// fR Edge
		b.add(cl.clone().add(6, 3, 6));
		b.add(cl.clone().add(6, 2, 6));
		b.add(cl.clone().add(6, 0, 6));
		b.add(cl.clone().add(6, 1, 6));
		b.add(cl.clone().add(6, -1, 6));
		b.add(cl.clone().add(6, 4, 6));
		b.add(cl.clone().add(6, 5, 6));
		b.add(cl.clone().add(6, 6, 6));
		b.add(cl.clone().add(6, 7, 6));
		b.add(cl.clone().add(6, 8, 6));
		b.add(cl.clone().add(6, 9, 6));

		// bL Edge
		b.add(cl.clone().add(-6, 3, -6));
		b.add(cl.clone().add(-6, 2, -6));
		b.add(cl.clone().add(-6, 0, -6));
		b.add(cl.clone().add(-6, 1, -6));
		b.add(cl.clone().add(-6, -1, -6));
		b.add(cl.clone().add(-6, 4, -6));
		b.add(cl.clone().add(-6, 5, -6));
		b.add(cl.clone().add(-6, 6, -6));
		b.add(cl.clone().add(-6, 7, -6));
		b.add(cl.clone().add(-6, 8, -6));
		b.add(cl.clone().add(-6, 9, -6));

		// bR Edge
		b.add(cl.clone().add(-6, 3, 6));
		b.add(cl.clone().add(-6, 2, 6));
		b.add(cl.clone().add(-6, 0, 6));
		b.add(cl.clone().add(-6, 1, 6));
		b.add(cl.clone().add(-6, -1, 6));
		b.add(cl.clone().add(-6, 4, 6));
		b.add(cl.clone().add(-6, 5, 6));
		b.add(cl.clone().add(-6, 6, 6));
		b.add(cl.clone().add(-6, 7, 6));
		b.add(cl.clone().add(-6, 8, 6));
		b.add(cl.clone().add(-6, 9, 6));

		// Bottom Corners
		b.add(cl.clone().add(6, -2, 6));
		b.add(cl.clone().add(-6, -2, 6));
		b.add(cl.clone().add(-6, -2, -6));
		b.add(cl.clone().add(6, -2, -6));

		// Top Corners
		b.add(cl.clone().add(6, 10, 6));
		b.add(cl.clone().add(-6, 10, 6));
		b.add(cl.clone().add(-6, 10, -6));
		b.add(cl.clone().add(6, 10, -6));

		for (final Location l : b) {
			center.put(l, cl);
			if (CobbleCubes.getInstance().allBorderBlocks.get(p.getUniqueId()) == null) {
				CobbleCubes.getInstance().allBorderBlocks.put(p.getUniqueId(), new ArrayList<Location>() {
					{
						add(l);
					}
				});
			} else {
				CobbleCubes.getInstance().allBorderBlocks.get(p.getUniqueId()).add(l);
			}
			new BukkitRunnable() {
				@Override
				public void run() {
					l.getBlock().setType(Material.BEDROCK);
				}
			}.runTaskLater(CobbleCubes.getInstance(), 5);
		}
	}

	@SuppressWarnings("serial")
	public void fBorder(Player p, Location cl) {
		ArrayList<Location> b = new ArrayList<Location>();

		cl.add(0, 2, 0);

		// bF Edge
		b.add(cl.clone().add(0, -2, 7));
		b.add(cl.clone().add(1, -2, 7));
		b.add(cl.clone().add(-1, -2, 7));
		b.add(cl.clone().add(2, -2, 7));
		b.add(cl.clone().add(-2, -2, 7));
		b.add(cl.clone().add(3, -2, 7));
		b.add(cl.clone().add(-3, -2, 7));
		b.add(cl.clone().add(4, -2, 7));
		b.add(cl.clone().add(-4, -2, 7));
		b.add(cl.clone().add(5, -2, 7));
		b.add(cl.clone().add(-5, -2, 7));
		b.add(cl.clone().add(6, -2, 7));
		b.add(cl.clone().add(-6, -2, 7));

		// bB Edge
		b.add(cl.clone().add(0, -2, -7));
		b.add(cl.clone().add(1, -2, -7));
		b.add(cl.clone().add(-1, -2, -7));
		b.add(cl.clone().add(2, -2, -7));
		b.add(cl.clone().add(-2, -2, -7));
		b.add(cl.clone().add(3, -2, -7));
		b.add(cl.clone().add(-3, -2, -7));
		b.add(cl.clone().add(4, -2, -7));
		b.add(cl.clone().add(-4, -2, -7));
		b.add(cl.clone().add(5, -2, -7));
		b.add(cl.clone().add(-5, -2, -7));
		b.add(cl.clone().add(6, -2, -7));
		b.add(cl.clone().add(-6, -2, -7));

		// bL Edge
		b.add(cl.clone().add(-7, -2, -1));
		b.add(cl.clone().add(-7, -2, 0));
		b.add(cl.clone().add(-7, -2, 1));
		b.add(cl.clone().add(-7, -2, -2));
		b.add(cl.clone().add(-7, -2, 2));
		b.add(cl.clone().add(-7, -2, -3));
		b.add(cl.clone().add(-7, -2, 3));
		b.add(cl.clone().add(-7, -2, -4));
		b.add(cl.clone().add(-7, -2, 4));
		b.add(cl.clone().add(-7, -2, -5));
		b.add(cl.clone().add(-7, -2, 5));
		b.add(cl.clone().add(-7, -2, -6));
		b.add(cl.clone().add(-7, -2, 6));

		// bR Edge
		b.add(cl.clone().add(7, -2, -1));
		b.add(cl.clone().add(7, -2, 0));
		b.add(cl.clone().add(7, -2, 1));
		b.add(cl.clone().add(7, -2, -2));
		b.add(cl.clone().add(7, -2, 2));
		b.add(cl.clone().add(7, -2, -3));
		b.add(cl.clone().add(7, -2, 3));
		b.add(cl.clone().add(7, -2, -4));
		b.add(cl.clone().add(7, -2, 4));
		b.add(cl.clone().add(7, -2, -5));
		b.add(cl.clone().add(7, -2, 5));
		b.add(cl.clone().add(7, -2, -6));
		b.add(cl.clone().add(7, -2, 6));

		// tF Edge
		b.add(cl.clone().add(0, 12, 7));
		b.add(cl.clone().add(1, 12, 7));
		b.add(cl.clone().add(-1, 12, 7));
		b.add(cl.clone().add(2, 12, 7));
		b.add(cl.clone().add(-2, 12, 7));
		b.add(cl.clone().add(3, 12, 7));
		b.add(cl.clone().add(-3, 12, 7));
		b.add(cl.clone().add(4, 12, 7));
		b.add(cl.clone().add(-4, 12, 7));
		b.add(cl.clone().add(5, 12, 7));
		b.add(cl.clone().add(-5, 12, 7));
		b.add(cl.clone().add(6, 12, 7));
		b.add(cl.clone().add(-6, 12, 7));

		// tB Edge
		b.add(cl.clone().add(0, 12, -7));
		b.add(cl.clone().add(1, 12, -7));
		b.add(cl.clone().add(-1, 12, -7));
		b.add(cl.clone().add(2, 12, -7));
		b.add(cl.clone().add(-2, 12, -7));
		b.add(cl.clone().add(3, 12, -7));
		b.add(cl.clone().add(-3, 12, -7));
		b.add(cl.clone().add(4, 12, -7));
		b.add(cl.clone().add(-4, 12, -7));
		b.add(cl.clone().add(5, 12, -7));
		b.add(cl.clone().add(-5, 12, -7));
		b.add(cl.clone().add(6, 12, -7));
		b.add(cl.clone().add(-6, 12, -7));

		// tL Edge
		b.add(cl.clone().add(-7, 12, -1));
		b.add(cl.clone().add(-7, 12, 0));
		b.add(cl.clone().add(-7, 12, 1));
		b.add(cl.clone().add(-7, 12, -2));
		b.add(cl.clone().add(-7, 12, 2));
		b.add(cl.clone().add(-7, 12, 3));
		b.add(cl.clone().add(-7, 12, -3));
		b.add(cl.clone().add(-7, 12, 4));
		b.add(cl.clone().add(-7, 12, -4));
		b.add(cl.clone().add(-7, 12, 5));
		b.add(cl.clone().add(-7, 12, -5));
		b.add(cl.clone().add(-7, 12, 6));
		b.add(cl.clone().add(-7, 12, -6));

		// tR Edge
		b.add(cl.clone().add(7, 12, -1));
		b.add(cl.clone().add(7, 12, 0));
		b.add(cl.clone().add(7, 12, 1));
		b.add(cl.clone().add(7, 12, -2));
		b.add(cl.clone().add(7, 12, 2));
		b.add(cl.clone().add(7, 12, -3));
		b.add(cl.clone().add(7, 12, 3));
		b.add(cl.clone().add(7, 12, -3));
		b.add(cl.clone().add(7, 12, 4));
		b.add(cl.clone().add(7, 12, -4));
		b.add(cl.clone().add(7, 12, 5));
		b.add(cl.clone().add(7, 12, -5));
		b.add(cl.clone().add(7, 12, 6));
		b.add(cl.clone().add(7, 12, -6));

		// fL Edge
		b.add(cl.clone().add(7, 3, -7));
		b.add(cl.clone().add(7, 2, -7));
		b.add(cl.clone().add(7, 0, -7));
		b.add(cl.clone().add(7, 1, -7));
		b.add(cl.clone().add(7, -1, -7));
		b.add(cl.clone().add(7, 4, -7));
		b.add(cl.clone().add(7, 5, -7));
		b.add(cl.clone().add(7, 6, -7));
		b.add(cl.clone().add(7, 7, -7));
		b.add(cl.clone().add(7, 8, -7));
		b.add(cl.clone().add(7, 9, -7));
		b.add(cl.clone().add(7, 10, -7));
		b.add(cl.clone().add(7, 11, -7));

		// fR Edge
		b.add(cl.clone().add(7, 3, 7));
		b.add(cl.clone().add(7, 2, 7));
		b.add(cl.clone().add(7, 0, 7));
		b.add(cl.clone().add(7, 1, 7));
		b.add(cl.clone().add(7, -1, 7));
		b.add(cl.clone().add(7, 4, 7));
		b.add(cl.clone().add(7, 5, 7));
		b.add(cl.clone().add(7, 6, 7));
		b.add(cl.clone().add(7, 7, 7));
		b.add(cl.clone().add(7, 8, 7));
		b.add(cl.clone().add(7, 9, 7));
		b.add(cl.clone().add(7, 10, 7));
		b.add(cl.clone().add(7, 11, 7));

		// bL Edge
		b.add(cl.clone().add(-7, 3, -7));
		b.add(cl.clone().add(-7, 2, -7));
		b.add(cl.clone().add(-7, 0, -7));
		b.add(cl.clone().add(-7, 1, -7));
		b.add(cl.clone().add(-7, -1, -7));
		b.add(cl.clone().add(-7, 4, -7));
		b.add(cl.clone().add(-7, 5, -7));
		b.add(cl.clone().add(-7, 6, -7));
		b.add(cl.clone().add(-7, 7, -7));
		b.add(cl.clone().add(-7, 8, -7));
		b.add(cl.clone().add(-7, 9, -7));
		b.add(cl.clone().add(-7, 10, -7));
		b.add(cl.clone().add(-7, 11, -7));

		// bR Edge
		b.add(cl.clone().add(-7, 3, 7));
		b.add(cl.clone().add(-7, 2, 7));
		b.add(cl.clone().add(-7, 0, 7));
		b.add(cl.clone().add(-7, 1, 7));
		b.add(cl.clone().add(-7, -1, 7));
		b.add(cl.clone().add(-7, 4, 7));
		b.add(cl.clone().add(-7, 5, 7));
		b.add(cl.clone().add(-7, 6, 7));
		b.add(cl.clone().add(-7, 7, 7));
		b.add(cl.clone().add(-7, 8, 7));
		b.add(cl.clone().add(-7, 9, 7));
		b.add(cl.clone().add(-7, 10, 7));
		b.add(cl.clone().add(-7, 11, 7));

		// Bottom Corners
		b.add(cl.clone().add(7, -2, 7));
		b.add(cl.clone().add(-7, -2, 7));
		b.add(cl.clone().add(-7, -2, -7));
		b.add(cl.clone().add(7, -2, -7));

		// Top Corners
		b.add(cl.clone().add(7, 12, 7));
		b.add(cl.clone().add(-7, 12, 7));
		b.add(cl.clone().add(-7, 12, -7));
		b.add(cl.clone().add(7, 12, -7));

		for (final Location l : b) {
			center.put(l, cl);
			if (CobbleCubes.getInstance().allBorderBlocks.get(p.getUniqueId()) == null) {
				CobbleCubes.getInstance().allBorderBlocks.put(p.getUniqueId(), new ArrayList<Location>() {
					{
						add(l);
					}
				});
			} else {
				CobbleCubes.getInstance().allBorderBlocks.get(p.getUniqueId()).add(l);
			}
			new BukkitRunnable() {
				@Override
				public void run() {
					l.getBlock().setType(Material.BEDROCK);
				}
			}.runTaskLater(CobbleCubes.getInstance(), 5);
		}
	}

	@SuppressWarnings("serial")
	public void gBorder(Player p, Location cl) {
		ArrayList<Location> b = new ArrayList<Location>();

		cl.add(0, 2, 0);

		// bF Edge
		b.add(cl.clone().add(0, -2, 8));
		b.add(cl.clone().add(1, -2, 8));
		b.add(cl.clone().add(-1, -2, 8));
		b.add(cl.clone().add(2, -2, 8));
		b.add(cl.clone().add(-2, -2, 8));
		b.add(cl.clone().add(3, -2, 8));
		b.add(cl.clone().add(-3, -2, 8));
		b.add(cl.clone().add(4, -2, 8));
		b.add(cl.clone().add(-4, -2, 8));
		b.add(cl.clone().add(5, -2, 8));
		b.add(cl.clone().add(-5, -2, 8));
		b.add(cl.clone().add(6, -2, 8));
		b.add(cl.clone().add(-6, -2, 8));

		// bB Edge
		b.add(cl.clone().add(0, -2, -8));
		b.add(cl.clone().add(1, -2, -8));
		b.add(cl.clone().add(-1, -2, -8));
		b.add(cl.clone().add(2, -2, -8));
		b.add(cl.clone().add(-2, -2, -8));
		b.add(cl.clone().add(3, -2, -8));
		b.add(cl.clone().add(-3, -2, -8));
		b.add(cl.clone().add(4, -2, -8));
		b.add(cl.clone().add(-4, -2, -8));
		b.add(cl.clone().add(5, -2, -8));
		b.add(cl.clone().add(-5, -2, -8));
		b.add(cl.clone().add(6, -2, -8));
		b.add(cl.clone().add(-6, -2, -8));

		// bL Edge
		b.add(cl.clone().add(-8, -2, -1));
		b.add(cl.clone().add(-8, -2, 0));
		b.add(cl.clone().add(-8, -2, 1));
		b.add(cl.clone().add(-8, -2, -2));
		b.add(cl.clone().add(-8, -2, 2));
		b.add(cl.clone().add(-8, -2, -3));
		b.add(cl.clone().add(-8, -2, 3));
		b.add(cl.clone().add(-8, -2, -4));
		b.add(cl.clone().add(-8, -2, 4));
		b.add(cl.clone().add(-8, -2, -5));
		b.add(cl.clone().add(-8, -2, 5));
		b.add(cl.clone().add(-8, -2, -6));
		b.add(cl.clone().add(-8, -2, 6));

		// bR Edge
		b.add(cl.clone().add(8, -2, -1));
		b.add(cl.clone().add(8, -2, 0));
		b.add(cl.clone().add(8, -2, 1));
		b.add(cl.clone().add(8, -2, -2));
		b.add(cl.clone().add(8, -2, 2));
		b.add(cl.clone().add(8, -2, -3));
		b.add(cl.clone().add(8, -2, 3));
		b.add(cl.clone().add(8, -2, -4));
		b.add(cl.clone().add(8, -2, 4));
		b.add(cl.clone().add(8, -2, -5));
		b.add(cl.clone().add(8, -2, 5));
		b.add(cl.clone().add(8, -2, -6));
		b.add(cl.clone().add(8, -2, 6));

		// tF Edge
		b.add(cl.clone().add(0, 12, 8));
		b.add(cl.clone().add(1, 12, 8));
		b.add(cl.clone().add(-1, 12, 8));
		b.add(cl.clone().add(2, 12, 8));
		b.add(cl.clone().add(-2, 12, 8));
		b.add(cl.clone().add(3, 12, 8));
		b.add(cl.clone().add(-3, 12, 8));
		b.add(cl.clone().add(4, 12, 8));
		b.add(cl.clone().add(-4, 12, 8));
		b.add(cl.clone().add(5, 12, 8));
		b.add(cl.clone().add(-5, 12, 8));
		b.add(cl.clone().add(6, 12, 8));
		b.add(cl.clone().add(-6, 12, 8));

		// tB Edge
		b.add(cl.clone().add(0, 12, -8));
		b.add(cl.clone().add(1, 12, -8));
		b.add(cl.clone().add(-1, 12, -8));
		b.add(cl.clone().add(2, 12, -8));
		b.add(cl.clone().add(-2, 12, -8));
		b.add(cl.clone().add(3, 12, -8));
		b.add(cl.clone().add(-3, 12, -8));
		b.add(cl.clone().add(4, 12, -8));
		b.add(cl.clone().add(-4, 12, -8));
		b.add(cl.clone().add(5, 12, -8));
		b.add(cl.clone().add(-5, 12, -8));
		b.add(cl.clone().add(6, 12, -8));
		b.add(cl.clone().add(-6, 12, -8));

		// tL Edge
		b.add(cl.clone().add(-8, 12, -1));
		b.add(cl.clone().add(-8, 12, 0));
		b.add(cl.clone().add(-8, 12, 1));
		b.add(cl.clone().add(-8, 12, -2));
		b.add(cl.clone().add(-8, 12, 2));
		b.add(cl.clone().add(-8, 12, 3));
		b.add(cl.clone().add(-8, 12, -3));
		b.add(cl.clone().add(-8, 12, 4));
		b.add(cl.clone().add(-8, 12, -4));
		b.add(cl.clone().add(-8, 12, 5));
		b.add(cl.clone().add(-8, 12, -5));
		b.add(cl.clone().add(-8, 12, 6));
		b.add(cl.clone().add(-8, 12, -6));

		// tR Edge
		b.add(cl.clone().add(8, 12, -1));
		b.add(cl.clone().add(8, 12, 0));
		b.add(cl.clone().add(8, 12, 1));
		b.add(cl.clone().add(8, 12, -2));
		b.add(cl.clone().add(8, 12, 2));
		b.add(cl.clone().add(8, 12, -3));
		b.add(cl.clone().add(8, 12, 3));
		b.add(cl.clone().add(8, 12, -3));
		b.add(cl.clone().add(8, 12, 4));
		b.add(cl.clone().add(8, 12, -4));
		b.add(cl.clone().add(8, 12, 5));
		b.add(cl.clone().add(8, 12, -5));
		b.add(cl.clone().add(8, 12, 6));
		b.add(cl.clone().add(8, 12, -6));

		// fL Edge
		b.add(cl.clone().add(8, 3, -8));
		b.add(cl.clone().add(8, 2, -8));
		b.add(cl.clone().add(8, 0, -8));
		b.add(cl.clone().add(8, 1, -8));
		b.add(cl.clone().add(8, -1, -8));
		b.add(cl.clone().add(8, 4, -8));
		b.add(cl.clone().add(8, 5, -8));
		b.add(cl.clone().add(8, 6, -8));
		b.add(cl.clone().add(8, 7, -8));
		b.add(cl.clone().add(8, 8, -8));
		b.add(cl.clone().add(8, 9, -8));
		b.add(cl.clone().add(8, 10, -8));
		b.add(cl.clone().add(8, 11, -8));

		// fR Edge
		b.add(cl.clone().add(8, 3, 8));
		b.add(cl.clone().add(8, 2, 8));
		b.add(cl.clone().add(8, 0, 8));
		b.add(cl.clone().add(8, 1, 8));
		b.add(cl.clone().add(8, -1, 8));
		b.add(cl.clone().add(8, 4, 8));
		b.add(cl.clone().add(8, 5, 8));
		b.add(cl.clone().add(8, 6, 8));
		b.add(cl.clone().add(8, 7, 8));
		b.add(cl.clone().add(8, 8, 8));
		b.add(cl.clone().add(8, 9, 8));
		b.add(cl.clone().add(8, 10, 8));
		b.add(cl.clone().add(8, 11, 8));

		// bL Edge
		b.add(cl.clone().add(-8, 3, -8));
		b.add(cl.clone().add(-8, 2, -8));
		b.add(cl.clone().add(-8, 0, -8));
		b.add(cl.clone().add(-8, 1, -8));
		b.add(cl.clone().add(-8, -1, -8));
		b.add(cl.clone().add(-8, 4, -8));
		b.add(cl.clone().add(-8, 5, -8));
		b.add(cl.clone().add(-8, 6, -8));
		b.add(cl.clone().add(-8, 7, -8));
		b.add(cl.clone().add(-8, 8, -8));
		b.add(cl.clone().add(-8, 9, -8));
		b.add(cl.clone().add(-8, 10, -8));
		b.add(cl.clone().add(-8, 11, -8));

		// bR Edge
		b.add(cl.clone().add(-8, 3, 8));
		b.add(cl.clone().add(-8, 2, 8));
		b.add(cl.clone().add(-8, 0, 8));
		b.add(cl.clone().add(-8, 1, 8));
		b.add(cl.clone().add(-8, -1, 8));
		b.add(cl.clone().add(-8, 4, 8));
		b.add(cl.clone().add(-8, 5, 8));
		b.add(cl.clone().add(-8, 6, 8));
		b.add(cl.clone().add(-8, 7, 8));
		b.add(cl.clone().add(-8, 8, 8));
		b.add(cl.clone().add(-8, 9, 8));
		b.add(cl.clone().add(-8, 10, 8));
		b.add(cl.clone().add(-8, 11, 8));

		// Bottom Corners
		b.add(cl.clone().add(8, -2, 8));
		b.add(cl.clone().add(-8, -2, 8));
		b.add(cl.clone().add(-8, -2, -8));
		b.add(cl.clone().add(8, -2, -8));

		// Top Corners
		b.add(cl.clone().add(8, 12, 8));
		b.add(cl.clone().add(-8, 12, 8));
		b.add(cl.clone().add(-8, 12, -8));
		b.add(cl.clone().add(8, 12, -8));

		for (final Location l : b) {
			center.put(l, cl);
			if (CobbleCubes.getInstance().allBorderBlocks.get(p.getUniqueId()) == null) {
				CobbleCubes.getInstance().allBorderBlocks.put(p.getUniqueId(), new ArrayList<Location>() {
					{
						add(l);
					}
				});
			} else {
				CobbleCubes.getInstance().allBorderBlocks.get(p.getUniqueId()).add(l);
			}
			new BukkitRunnable() {
				@Override
				public void run() {
					l.getBlock().setType(Material.BEDROCK);
				}
			}.runTaskLater(CobbleCubes.getInstance(), 5);
		}
	}

	public static void removeACube(Player p, Location cl) {
		PlayerData data = new PlayerData(p.getUniqueId());
		data.removeGenerator(cl);
		for (int tx = cl.getBlockX() - 1; tx <= cl.getBlockX() + 1; ++tx) {
			for (int ty = cl.getBlockY() - 1; ty <= cl.getBlockY() + 1; ++ty) {
				for (int tz = cl.getBlockZ() - 1; tz <= cl.getBlockZ() + 1; ++tz) {

					final Integer x = tx;
					final Integer y = ty;
					final Integer z = tz;
					CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId())
							.remove(cl.getWorld().getBlockAt(x, y, z).getLocation());
					center.remove(cl.getWorld().getBlockAt(x, y, z).getLocation());
					cl.getWorld().getBlockAt(x, y, z).setType(Material.AIR);
					type.remove(cl.getWorld().getBlockAt(x, y, z).getLocation());
				}
			}
		}

		ArrayList<Location> b = new ArrayList<Location>();

		// bF Edge
		b.add(cl.clone().add(0, -2, 2));
		b.add(cl.clone().add(1, -2, 2));
		b.add(cl.clone().add(-1, -2, 2));

		// bB Edge
		b.add(cl.clone().add(0, -2, -2));
		b.add(cl.clone().add(1, -2, -2));
		b.add(cl.clone().add(-1, -2, -2));

		// bL Edge
		b.add(cl.clone().add(-2, -2, -1));
		b.add(cl.clone().add(-2, -2, 0));
		b.add(cl.clone().add(-2, -2, 1));

		// bR Edge
		b.add(cl.clone().add(2, -2, -1));
		b.add(cl.clone().add(2, -2, 0));
		b.add(cl.clone().add(2, -2, 1));

		// tF Edge
		b.add(cl.clone().add(0, 2, 2));
		b.add(cl.clone().add(1, 2, 2));
		b.add(cl.clone().add(-1, 2, 2));

		// tB Edge
		b.add(cl.clone().add(0, 2, -2));
		b.add(cl.clone().add(1, 2, -2));
		b.add(cl.clone().add(-1, 2, -2));

		// tL Edge
		b.add(cl.clone().add(-2, 2, -1));
		b.add(cl.clone().add(-2, 2, 0));
		b.add(cl.clone().add(-2, 2, 1));

		// tR Edge
		b.add(cl.clone().add(2, 2, -1));
		b.add(cl.clone().add(2, 2, 0));
		b.add(cl.clone().add(2, 2, 1));

		// fL Edge
		b.add(cl.clone().add(2, 0, -2));
		b.add(cl.clone().add(2, 1, -2));
		b.add(cl.clone().add(2, -1, -2));

		// fR Edge
		b.add(cl.clone().add(2, 0, 2));
		b.add(cl.clone().add(2, 1, 2));
		b.add(cl.clone().add(2, -1, 2));

		// bL Edge
		b.add(cl.clone().add(-2, 0, -2));
		b.add(cl.clone().add(-2, 1, -2));
		b.add(cl.clone().add(-2, -1, -2));

		// bR Edge
		b.add(cl.clone().add(-2, 0, 2));
		b.add(cl.clone().add(-2, 1, 2));
		b.add(cl.clone().add(-2, -1, 2));

		// Bottom Corners
		b.add(cl.clone().add(2, -2, 2));
		b.add(cl.clone().add(-2, -2, 2));
		b.add(cl.clone().add(-2, -2, -2));
		b.add(cl.clone().add(2, -2, -2));

		// Top Corners
		b.add(cl.clone().add(2, 2, 2));
		b.add(cl.clone().add(-2, 2, 2));
		b.add(cl.clone().add(-2, 2, -2));
		b.add(cl.clone().add(2, 2, -2));

		for (final Location l : b) {
			new BukkitRunnable() {
				@Override
				public void run() {
					l.getBlock().setType(Material.AIR);
				}
			}.runTaskLater(CobbleCubes.getInstance(), 15);
		}
	}

	public static void removeBCube(Player p, Location c) {
		PlayerData data = new PlayerData(p.getUniqueId());
		data.removeGenerator(c);
		for (int tx = c.getBlockX() - 2; tx <= c.getBlockX() + 2; ++tx) {
			for (int ty = c.getBlockY() - 2; ty <= c.getBlockY() + 2; ++ty) {
				for (int tz = c.getBlockZ() - 2; tz <= c.getBlockZ() + 2; ++tz) {

					final Integer x = tx;
					final Integer y = ty;
					final Integer z = tz;
					CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId())
							.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
					center.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
					c.getWorld().getBlockAt(x, y, z).setType(Material.AIR);
					type.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
				}
			}
		}

		Location cl = c.add(0, -1, 0);
		ArrayList<Location> b = new ArrayList<Location>();

		// bF Edge
		b.add(cl.clone().add(0, -2, 3));
		b.add(cl.clone().add(1, -2, 3));
		b.add(cl.clone().add(-1, -2, 3));
		b.add(cl.clone().add(2, -2, 3));
		b.add(cl.clone().add(-2, -2, 3));

		// bB Edge
		b.add(cl.clone().add(0, -2, -3));
		b.add(cl.clone().add(1, -2, -3));
		b.add(cl.clone().add(-1, -2, -3));
		b.add(cl.clone().add(2, -2, -3));
		b.add(cl.clone().add(-2, -2, -3));

		// bL Edge
		b.add(cl.clone().add(-3, -2, -1));
		b.add(cl.clone().add(-3, -2, 0));
		b.add(cl.clone().add(-3, -2, 1));
		b.add(cl.clone().add(-3, -2, -2));
		b.add(cl.clone().add(-3, -2, 2));

		// bR Edge
		b.add(cl.clone().add(3, -2, -1));
		b.add(cl.clone().add(3, -2, 0));
		b.add(cl.clone().add(3, -2, 1));
		b.add(cl.clone().add(3, -2, -2));
		b.add(cl.clone().add(3, -2, 2));

		// tF Edge
		b.add(cl.clone().add(0, 4, 3));
		b.add(cl.clone().add(1, 4, 3));
		b.add(cl.clone().add(-1, 4, 3));
		b.add(cl.clone().add(2, 4, 3));
		b.add(cl.clone().add(-2, 4, 3));

		// tB Edge
		b.add(cl.clone().add(0, 4, -3));
		b.add(cl.clone().add(1, 4, -3));
		b.add(cl.clone().add(-1, 4, -3));
		b.add(cl.clone().add(2, 4, -3));
		b.add(cl.clone().add(-2, 4, -3));

		// tL Edge
		b.add(cl.clone().add(-3, 4, -1));
		b.add(cl.clone().add(-3, 4, 0));
		b.add(cl.clone().add(-3, 4, 1));
		b.add(cl.clone().add(-3, 4, -2));
		b.add(cl.clone().add(-3, 4, 2));

		// tR Edge
		b.add(cl.clone().add(3, 4, -1));
		b.add(cl.clone().add(3, 4, 0));
		b.add(cl.clone().add(3, 4, 1));
		b.add(cl.clone().add(3, 4, -2));
		b.add(cl.clone().add(3, 4, 2));

		// fL Edge
		b.add(cl.clone().add(3, 3, -3));
		b.add(cl.clone().add(3, 2, -3));
		b.add(cl.clone().add(3, 0, -3));
		b.add(cl.clone().add(3, 1, -3));
		b.add(cl.clone().add(3, -1, -3));

		// fR Edge
		b.add(cl.clone().add(3, 3, 3));
		b.add(cl.clone().add(3, 2, 3));
		b.add(cl.clone().add(3, 0, 3));
		b.add(cl.clone().add(3, 1, 3));
		b.add(cl.clone().add(3, -1, 3));

		// bL Edge
		b.add(cl.clone().add(-3, 3, -3));
		b.add(cl.clone().add(-3, 2, -3));
		b.add(cl.clone().add(-3, 0, -3));
		b.add(cl.clone().add(-3, 1, -3));
		b.add(cl.clone().add(-3, -1, -3));

		// bR Edge
		b.add(cl.clone().add(-3, 3, 3));
		b.add(cl.clone().add(-3, 2, 3));
		b.add(cl.clone().add(-3, 0, 3));
		b.add(cl.clone().add(-3, 1, 3));
		b.add(cl.clone().add(-3, -1, 3));

		// Bottom Corners
		b.add(cl.clone().add(3, -2, 3));
		b.add(cl.clone().add(-3, -2, 3));
		b.add(cl.clone().add(-3, -2, -3));
		b.add(cl.clone().add(3, -2, -3));

		// Top Corners
		b.add(cl.clone().add(3, 4, 3));
		b.add(cl.clone().add(-3, 4, 3));
		b.add(cl.clone().add(-3, 4, -3));
		b.add(cl.clone().add(3, 4, -3));

		for (final Location l : b) {
			new BukkitRunnable() {
				@Override
				public void run() {
					l.getBlock().setType(Material.AIR);
				}
			}.runTaskLater(CobbleCubes.getInstance(), 15);
		}
	}

	public static void removeCCube(Player p, Location c) {
		PlayerData data = new PlayerData(p.getUniqueId());
		data.removeGenerator(c);
		for (int tx = c.getBlockX() - 3; tx <= c.getBlockX() + 3; ++tx) {
			for (int ty = c.getBlockY() - 3; ty <= c.getBlockY() + 3; ++ty) {
				for (int tz = c.getBlockZ() - 3; tz <= c.getBlockZ() + 3; ++tz) {

					final Integer x = tx;
					final Integer y = ty;
					final Integer z = tz;
					CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId())
							.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
					center.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
					c.getWorld().getBlockAt(x, y, z).setType(Material.AIR);
					type.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
				}
			}
		}

		Location cl = c.add(0, -2, 0);
		ArrayList<Location> b = new ArrayList<Location>();

		// bF Edge
		b.add(cl.clone().add(0, -2, 4));
		b.add(cl.clone().add(1, -2, 4));
		b.add(cl.clone().add(-1, -2, 4));
		b.add(cl.clone().add(2, -2, 4));
		b.add(cl.clone().add(-2, -2, 4));
		b.add(cl.clone().add(3, -2, 4));
		b.add(cl.clone().add(-3, -2, 4));

		// bB Edge
		b.add(cl.clone().add(0, -2, -4));
		b.add(cl.clone().add(1, -2, -4));
		b.add(cl.clone().add(-1, -2, -4));
		b.add(cl.clone().add(2, -2, -4));
		b.add(cl.clone().add(-2, -2, -4));
		b.add(cl.clone().add(3, -2, -4));
		b.add(cl.clone().add(-3, -2, -4));

		// bL Edge
		b.add(cl.clone().add(-4, -2, -1));
		b.add(cl.clone().add(-4, -2, 0));
		b.add(cl.clone().add(-4, -2, 1));
		b.add(cl.clone().add(-4, -2, -2));
		b.add(cl.clone().add(-4, -2, 2));
		b.add(cl.clone().add(-4, -2, -3));
		b.add(cl.clone().add(-4, -2, 3));

		// bR Edge
		b.add(cl.clone().add(4, -2, -1));
		b.add(cl.clone().add(4, -2, 0));
		b.add(cl.clone().add(4, -2, 1));
		b.add(cl.clone().add(4, -2, -2));
		b.add(cl.clone().add(4, -2, 2));
		b.add(cl.clone().add(4, -2, -3));
		b.add(cl.clone().add(4, -2, 3));

		// tF Edge
		b.add(cl.clone().add(0, 6, 4));
		b.add(cl.clone().add(1, 6, 4));
		b.add(cl.clone().add(-1, 6, 4));
		b.add(cl.clone().add(2, 6, 4));
		b.add(cl.clone().add(-2, 6, 4));
		b.add(cl.clone().add(3, 6, 4));
		b.add(cl.clone().add(-3, 6, 4));

		// tB Edge
		b.add(cl.clone().add(0, 6, -4));
		b.add(cl.clone().add(1, 6, -4));
		b.add(cl.clone().add(-1, 6, -4));
		b.add(cl.clone().add(2, 6, -4));
		b.add(cl.clone().add(-2, 6, -4));
		b.add(cl.clone().add(3, 6, -4));
		b.add(cl.clone().add(-3, 6, -4));

		// tL Edge
		b.add(cl.clone().add(-4, 6, -1));
		b.add(cl.clone().add(-4, 6, 0));
		b.add(cl.clone().add(-4, 6, 1));
		b.add(cl.clone().add(-4, 6, -2));
		b.add(cl.clone().add(-4, 6, 2));
		b.add(cl.clone().add(-4, 6, 3));
		b.add(cl.clone().add(-4, 6, -3));

		// tR Edge
		b.add(cl.clone().add(4, 6, -1));
		b.add(cl.clone().add(4, 6, 0));
		b.add(cl.clone().add(4, 6, 1));
		b.add(cl.clone().add(4, 6, -2));
		b.add(cl.clone().add(4, 6, 2));
		b.add(cl.clone().add(4, 6, -3));
		b.add(cl.clone().add(4, 6, 3));

		// fL Edge
		b.add(cl.clone().add(4, 3, -4));
		b.add(cl.clone().add(4, 2, -4));
		b.add(cl.clone().add(4, 0, -4));
		b.add(cl.clone().add(4, 1, -4));
		b.add(cl.clone().add(4, -1, -4));
		b.add(cl.clone().add(4, 4, -4));
		b.add(cl.clone().add(4, 5, -4));

		// fR Edge
		b.add(cl.clone().add(4, 3, 4));
		b.add(cl.clone().add(4, 2, 4));
		b.add(cl.clone().add(4, 0, 4));
		b.add(cl.clone().add(4, 1, 4));
		b.add(cl.clone().add(4, -1, 4));
		b.add(cl.clone().add(4, 4, 4));
		b.add(cl.clone().add(4, 5, 4));

		// bL Edge
		b.add(cl.clone().add(-4, 3, -4));
		b.add(cl.clone().add(-4, 2, -4));
		b.add(cl.clone().add(-4, 0, -4));
		b.add(cl.clone().add(-4, 1, -4));
		b.add(cl.clone().add(-4, -1, -4));
		b.add(cl.clone().add(-4, 4, -4));
		b.add(cl.clone().add(-4, 5, -4));

		// bR Edge
		b.add(cl.clone().add(-4, 3, 4));
		b.add(cl.clone().add(-4, 2, 4));
		b.add(cl.clone().add(-4, 0, 4));
		b.add(cl.clone().add(-4, 1, 4));
		b.add(cl.clone().add(-4, -1, 4));
		b.add(cl.clone().add(-4, 4, 4));
		b.add(cl.clone().add(-4, 5, 4));

		// Bottom Corners
		b.add(cl.clone().add(4, -2, 4));
		b.add(cl.clone().add(-4, -2, 4));
		b.add(cl.clone().add(-4, -2, -4));
		b.add(cl.clone().add(4, -2, -4));

		// Top Corners
		b.add(cl.clone().add(4, 6, 4));
		b.add(cl.clone().add(-4, 6, 4));
		b.add(cl.clone().add(-4, 6, -4));
		b.add(cl.clone().add(4, 6, -4));

		for (final Location l : b) {
			new BukkitRunnable() {
				@Override
				public void run() {
					l.getBlock().setType(Material.AIR);
				}
			}.runTaskLater(CobbleCubes.getInstance(), 15);
		}
	}

	public static void removeDCube(Player p, Location c) {
		PlayerData data = new PlayerData(p.getUniqueId());
		data.removeGenerator(c);
		for (int tx = c.getBlockX() - 4; tx <= c.getBlockX() + 4; ++tx) {
			for (int ty = c.getBlockY() - 4; ty <= c.getBlockY() + 4; ++ty) {
				for (int tz = c.getBlockZ() - 4; tz <= c.getBlockZ() + 4; ++tz) {

					final Integer x = tx;
					final Integer y = ty;
					final Integer z = tz;
					CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId())
							.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
					center.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
					c.getWorld().getBlockAt(x, y, z).setType(Material.AIR);
					type.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
				}
			}
		}

		Location cl = c.add(0, -3, 0);
		ArrayList<Location> b = new ArrayList<Location>();

		// bF Edge
		b.add(cl.clone().add(0, -2, 5));
		b.add(cl.clone().add(1, -2, 5));
		b.add(cl.clone().add(-1, -2, 5));
		b.add(cl.clone().add(2, -2, 5));
		b.add(cl.clone().add(-2, -2, 5));
		b.add(cl.clone().add(3, -2, 5));
		b.add(cl.clone().add(-3, -2, 5));
		b.add(cl.clone().add(4, -2, 5));
		b.add(cl.clone().add(-4, -2, 5));

		// bB Edge
		b.add(cl.clone().add(0, -2, -5));
		b.add(cl.clone().add(1, -2, -5));
		b.add(cl.clone().add(-1, -2, -5));
		b.add(cl.clone().add(2, -2, -5));
		b.add(cl.clone().add(-2, -2, -5));
		b.add(cl.clone().add(3, -2, -5));
		b.add(cl.clone().add(-3, -2, -5));
		b.add(cl.clone().add(4, -2, -5));
		b.add(cl.clone().add(-4, -2, -5));

		// bL Edge
		b.add(cl.clone().add(-5, -2, -1));
		b.add(cl.clone().add(-5, -2, 0));
		b.add(cl.clone().add(-5, -2, 1));
		b.add(cl.clone().add(-5, -2, -2));
		b.add(cl.clone().add(-5, -2, 2));
		b.add(cl.clone().add(-5, -2, -3));
		b.add(cl.clone().add(-5, -2, 3));
		b.add(cl.clone().add(-5, -2, -4));
		b.add(cl.clone().add(-5, -2, 4));

		// bR Edge
		b.add(cl.clone().add(5, -2, -1));
		b.add(cl.clone().add(5, -2, 0));
		b.add(cl.clone().add(5, -2, 1));
		b.add(cl.clone().add(5, -2, -2));
		b.add(cl.clone().add(5, -2, 2));
		b.add(cl.clone().add(5, -2, -3));
		b.add(cl.clone().add(5, -2, 3));
		b.add(cl.clone().add(5, -2, -4));
		b.add(cl.clone().add(5, -2, 4));

		// tF Edge
		b.add(cl.clone().add(0, 8, 5));
		b.add(cl.clone().add(1, 8, 5));
		b.add(cl.clone().add(-1, 8, 5));
		b.add(cl.clone().add(2, 8, 5));
		b.add(cl.clone().add(-2, 8, 5));
		b.add(cl.clone().add(3, 8, 5));
		b.add(cl.clone().add(-3, 8, 5));
		b.add(cl.clone().add(4, 8, 5));
		b.add(cl.clone().add(-4, 8, 5));

		// tB Edge
		b.add(cl.clone().add(0, 8, -5));
		b.add(cl.clone().add(1, 8, -5));
		b.add(cl.clone().add(-1, 8, -5));
		b.add(cl.clone().add(2, 8, -5));
		b.add(cl.clone().add(-2, 8, -5));
		b.add(cl.clone().add(3, 8, -5));
		b.add(cl.clone().add(-3, 8, -5));
		b.add(cl.clone().add(4, 8, -5));
		b.add(cl.clone().add(-4, 8, -5));

		// tL Edge
		b.add(cl.clone().add(-5, 8, -1));
		b.add(cl.clone().add(-5, 8, 0));
		b.add(cl.clone().add(-5, 8, 1));
		b.add(cl.clone().add(-5, 8, -2));
		b.add(cl.clone().add(-5, 8, 2));
		b.add(cl.clone().add(-5, 8, 3));
		b.add(cl.clone().add(-5, 8, -3));
		b.add(cl.clone().add(-5, 8, 4));
		b.add(cl.clone().add(-5, 8, -4));

		// tR Edge
		b.add(cl.clone().add(5, 8, -1));
		b.add(cl.clone().add(5, 8, 0));
		b.add(cl.clone().add(5, 8, 1));
		b.add(cl.clone().add(5, 8, -2));
		b.add(cl.clone().add(5, 8, 2));
		b.add(cl.clone().add(5, 8, -3));
		b.add(cl.clone().add(5, 8, 3));
		b.add(cl.clone().add(5, 8, -3));
		b.add(cl.clone().add(5, 8, 4));
		b.add(cl.clone().add(5, 8, -4));

		// fL Edge
		b.add(cl.clone().add(5, 3, -5));
		b.add(cl.clone().add(5, 2, -5));
		b.add(cl.clone().add(5, 0, -5));
		b.add(cl.clone().add(5, 1, -5));
		b.add(cl.clone().add(5, -1, -5));
		b.add(cl.clone().add(5, 4, -5));
		b.add(cl.clone().add(5, 5, -5));
		b.add(cl.clone().add(5, 6, -5));
		b.add(cl.clone().add(5, 7, -5));

		// fR Edge
		b.add(cl.clone().add(5, 3, 5));
		b.add(cl.clone().add(5, 2, 5));
		b.add(cl.clone().add(5, 0, 5));
		b.add(cl.clone().add(5, 1, 5));
		b.add(cl.clone().add(5, -1, 5));
		b.add(cl.clone().add(5, 4, 5));
		b.add(cl.clone().add(5, 5, 5));
		b.add(cl.clone().add(5, 6, 5));
		b.add(cl.clone().add(5, 7, 5));

		// bL Edge
		b.add(cl.clone().add(-5, 3, -5));
		b.add(cl.clone().add(-5, 2, -5));
		b.add(cl.clone().add(-5, 0, -5));
		b.add(cl.clone().add(-5, 1, -5));
		b.add(cl.clone().add(-5, -1, -5));
		b.add(cl.clone().add(-5, 4, -5));
		b.add(cl.clone().add(-5, 5, -5));
		b.add(cl.clone().add(-5, 6, -5));
		b.add(cl.clone().add(-5, 7, -5));

		// bR Edge
		b.add(cl.clone().add(-5, 3, 5));
		b.add(cl.clone().add(-5, 2, 5));
		b.add(cl.clone().add(-5, 0, 5));
		b.add(cl.clone().add(-5, 1, 5));
		b.add(cl.clone().add(-5, -1, 5));
		b.add(cl.clone().add(-5, 4, 5));
		b.add(cl.clone().add(-5, 5, 5));
		b.add(cl.clone().add(-5, 6, 5));
		b.add(cl.clone().add(-5, 7, 5));

		// Bottom Corners
		b.add(cl.clone().add(5, -2, 5));
		b.add(cl.clone().add(-5, -2, 5));
		b.add(cl.clone().add(-5, -2, -5));
		b.add(cl.clone().add(5, -2, -5));

		// Top Corners
		b.add(cl.clone().add(5, 8, 5));
		b.add(cl.clone().add(-5, 8, 5));
		b.add(cl.clone().add(-5, 8, -5));
		b.add(cl.clone().add(5, 8, -5));

		for (final Location l : b) {
			new BukkitRunnable() {
				@Override
				public void run() {
					l.getBlock().setType(Material.AIR);
				}
			}.runTaskLater(CobbleCubes.getInstance(), 15);
		}
	}

	public static void removeECube(Player p, Location c) {
		PlayerData data = new PlayerData(p.getUniqueId());
		data.removeGenerator(c);
		for (int tx = c.getBlockX() - 5; tx <= c.getBlockX() + 5; ++tx) {
			for (int ty = c.getBlockY() - 5; ty <= c.getBlockY() + 5; ++ty) {
				for (int tz = c.getBlockZ() - 5; tz <= c.getBlockZ() + 5; ++tz) {

					final Integer x = tx;
					final Integer y = ty;
					final Integer z = tz;
					CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId())
							.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
					center.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
					c.getWorld().getBlockAt(x, y, z).setType(Material.AIR);
					type.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
				}
			}
		}

		Location cl = c.add(0, -4, 0);
		ArrayList<Location> b = new ArrayList<Location>();

		// bF Edge
		b.add(cl.clone().add(0, -2, 6));
		b.add(cl.clone().add(1, -2, 6));
		b.add(cl.clone().add(-1, -2, 6));
		b.add(cl.clone().add(2, -2, 6));
		b.add(cl.clone().add(-2, -2, 6));
		b.add(cl.clone().add(3, -2, 6));
		b.add(cl.clone().add(-3, -2, 6));
		b.add(cl.clone().add(4, -2, 6));
		b.add(cl.clone().add(-4, -2, 6));
		b.add(cl.clone().add(5, -2, 6));
		b.add(cl.clone().add(-5, -2, 6));

		// bB Edge
		b.add(cl.clone().add(0, -2, -6));
		b.add(cl.clone().add(1, -2, -6));
		b.add(cl.clone().add(-1, -2, -6));
		b.add(cl.clone().add(2, -2, -6));
		b.add(cl.clone().add(-2, -2, -6));
		b.add(cl.clone().add(3, -2, -6));
		b.add(cl.clone().add(-3, -2, -6));
		b.add(cl.clone().add(4, -2, -6));
		b.add(cl.clone().add(-4, -2, -6));
		b.add(cl.clone().add(5, -2, -6));
		b.add(cl.clone().add(-5, -2, -6));

		// bL Edge
		b.add(cl.clone().add(-6, -2, -1));
		b.add(cl.clone().add(-6, -2, 0));
		b.add(cl.clone().add(-6, -2, 1));
		b.add(cl.clone().add(-6, -2, -2));
		b.add(cl.clone().add(-6, -2, 2));
		b.add(cl.clone().add(-6, -2, -3));
		b.add(cl.clone().add(-6, -2, 3));
		b.add(cl.clone().add(-6, -2, -4));
		b.add(cl.clone().add(-6, -2, 4));
		b.add(cl.clone().add(-6, -2, -5));
		b.add(cl.clone().add(-6, -2, 5));

		// bR Edge
		b.add(cl.clone().add(6, -2, -1));
		b.add(cl.clone().add(6, -2, 0));
		b.add(cl.clone().add(6, -2, 1));
		b.add(cl.clone().add(6, -2, -2));
		b.add(cl.clone().add(6, -2, 2));
		b.add(cl.clone().add(6, -2, -3));
		b.add(cl.clone().add(6, -2, 3));
		b.add(cl.clone().add(6, -2, -4));
		b.add(cl.clone().add(6, -2, 4));
		b.add(cl.clone().add(6, -2, -5));
		b.add(cl.clone().add(6, -2, 5));

		// tF Edge
		b.add(cl.clone().add(0, 10, 6));
		b.add(cl.clone().add(1, 10, 6));
		b.add(cl.clone().add(-1, 10, 6));
		b.add(cl.clone().add(2, 10, 6));
		b.add(cl.clone().add(-2, 10, 6));
		b.add(cl.clone().add(3, 10, 6));
		b.add(cl.clone().add(-3, 10, 6));
		b.add(cl.clone().add(4, 10, 6));
		b.add(cl.clone().add(-4, 10, 6));
		b.add(cl.clone().add(5, 10, 6));
		b.add(cl.clone().add(-5, 10, 6));

		// tB Edge
		b.add(cl.clone().add(0, 10, -6));
		b.add(cl.clone().add(1, 10, -6));
		b.add(cl.clone().add(-1, 10, -6));
		b.add(cl.clone().add(2, 10, -6));
		b.add(cl.clone().add(-2, 10, -6));
		b.add(cl.clone().add(3, 10, -6));
		b.add(cl.clone().add(-3, 10, -6));
		b.add(cl.clone().add(4, 10, -6));
		b.add(cl.clone().add(-4, 10, -6));
		b.add(cl.clone().add(5, 10, -6));
		b.add(cl.clone().add(-5, 10, -6));

		// tL Edge
		b.add(cl.clone().add(-6, 10, -1));
		b.add(cl.clone().add(-6, 10, 0));
		b.add(cl.clone().add(-6, 10, 1));
		b.add(cl.clone().add(-6, 10, -2));
		b.add(cl.clone().add(-6, 10, 2));
		b.add(cl.clone().add(-6, 10, 3));
		b.add(cl.clone().add(-6, 10, -3));
		b.add(cl.clone().add(-6, 10, 4));
		b.add(cl.clone().add(-6, 10, -4));
		b.add(cl.clone().add(-6, 10, 5));
		b.add(cl.clone().add(-6, 10, -5));

		// tR Edge
		b.add(cl.clone().add(6, 10, -1));
		b.add(cl.clone().add(6, 10, 0));
		b.add(cl.clone().add(6, 10, 1));
		b.add(cl.clone().add(6, 10, -2));
		b.add(cl.clone().add(6, 10, 2));
		b.add(cl.clone().add(6, 10, -3));
		b.add(cl.clone().add(6, 10, 3));
		b.add(cl.clone().add(6, 10, -3));
		b.add(cl.clone().add(6, 10, 4));
		b.add(cl.clone().add(6, 10, -4));
		b.add(cl.clone().add(6, 10, 5));
		b.add(cl.clone().add(6, 10, -5));

		// fL Edge
		b.add(cl.clone().add(6, 3, -6));
		b.add(cl.clone().add(6, 2, -6));
		b.add(cl.clone().add(6, 0, -6));
		b.add(cl.clone().add(6, 1, -6));
		b.add(cl.clone().add(6, -1, -6));
		b.add(cl.clone().add(6, 4, -6));
		b.add(cl.clone().add(6, 5, -6));
		b.add(cl.clone().add(6, 6, -6));
		b.add(cl.clone().add(6, 7, -6));
		b.add(cl.clone().add(6, 8, -6));
		b.add(cl.clone().add(6, 9, -6));

		// fR Edge
		b.add(cl.clone().add(6, 3, 6));
		b.add(cl.clone().add(6, 2, 6));
		b.add(cl.clone().add(6, 0, 6));
		b.add(cl.clone().add(6, 1, 6));
		b.add(cl.clone().add(6, -1, 6));
		b.add(cl.clone().add(6, 4, 6));
		b.add(cl.clone().add(6, 5, 6));
		b.add(cl.clone().add(6, 6, 6));
		b.add(cl.clone().add(6, 7, 6));
		b.add(cl.clone().add(6, 8, 6));
		b.add(cl.clone().add(6, 9, 6));

		// bL Edge
		b.add(cl.clone().add(-6, 3, -6));
		b.add(cl.clone().add(-6, 2, -6));
		b.add(cl.clone().add(-6, 0, -6));
		b.add(cl.clone().add(-6, 1, -6));
		b.add(cl.clone().add(-6, -1, -6));
		b.add(cl.clone().add(-6, 4, -6));
		b.add(cl.clone().add(-6, 5, -6));
		b.add(cl.clone().add(-6, 6, -6));
		b.add(cl.clone().add(-6, 7, -6));
		b.add(cl.clone().add(-6, 8, -6));
		b.add(cl.clone().add(-6, 9, -6));

		// bR Edge
		b.add(cl.clone().add(-6, 3, 6));
		b.add(cl.clone().add(-6, 2, 6));
		b.add(cl.clone().add(-6, 0, 6));
		b.add(cl.clone().add(-6, 1, 6));
		b.add(cl.clone().add(-6, -1, 6));
		b.add(cl.clone().add(-6, 4, 6));
		b.add(cl.clone().add(-6, 5, 6));
		b.add(cl.clone().add(-6, 6, 6));
		b.add(cl.clone().add(-6, 7, 6));
		b.add(cl.clone().add(-6, 8, 6));
		b.add(cl.clone().add(-6, 9, 6));

		// Bottom Corners
		b.add(cl.clone().add(6, -2, 6));
		b.add(cl.clone().add(-6, -2, 6));
		b.add(cl.clone().add(-6, -2, -6));
		b.add(cl.clone().add(6, -2, -6));

		// Top Corners
		b.add(cl.clone().add(6, 10, 6));
		b.add(cl.clone().add(-6, 10, 6));
		b.add(cl.clone().add(-6, 10, -6));
		b.add(cl.clone().add(6, 10, -6));

		for (final Location l : b) {
			new BukkitRunnable() {
				@Override
				public void run() {
					l.getBlock().setType(Material.AIR);
				}
			}.runTaskLater(CobbleCubes.getInstance(), 15);
		}
	}

	public static void removeFCube(Player p, Location c) {
		PlayerData data = new PlayerData(p.getUniqueId());
		data.removeGenerator(c);
		for (int tx = c.getBlockX() - 6; tx <= c.getBlockX() + 6; ++tx) {
			for (int ty = c.getBlockY() - 6; ty <= c.getBlockY() + 6; ++ty) {
				for (int tz = c.getBlockZ() - 6; tz <= c.getBlockZ() + 6; ++tz) {

					final Integer x = tx;
					final Integer y = ty;
					final Integer z = tz;
					
					CobbleCubes.getInstance().allGeneratorBlocks.get(p.getUniqueId())
							.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
					center.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
					c.getWorld().getBlockAt(x, y, z).setType(Material.AIR);
					type.remove(c.getWorld().getBlockAt(x, y, z).getLocation());
				}
			}
		}

		Location cl = c.add(0, -5, 0);
		ArrayList<Location> b = new ArrayList<Location>();

		// bF Edge
		b.add(cl.clone().add(0, -2, 7));
		b.add(cl.clone().add(1, -2, 7));
		b.add(cl.clone().add(-1, -2, 7));
		b.add(cl.clone().add(2, -2, 7));
		b.add(cl.clone().add(-2, -2, 7));
		b.add(cl.clone().add(3, -2, 7));
		b.add(cl.clone().add(-3, -2, 7));
		b.add(cl.clone().add(4, -2, 7));
		b.add(cl.clone().add(-4, -2, 7));
		b.add(cl.clone().add(5, -2, 7));
		b.add(cl.clone().add(-5, -2, 7));
		b.add(cl.clone().add(6, -2, 7));
		b.add(cl.clone().add(-6, -2, 7));

		// bB Edge
		b.add(cl.clone().add(0, -2, -7));
		b.add(cl.clone().add(1, -2, -7));
		b.add(cl.clone().add(-1, -2, -7));
		b.add(cl.clone().add(2, -2, -7));
		b.add(cl.clone().add(-2, -2, -7));
		b.add(cl.clone().add(3, -2, -7));
		b.add(cl.clone().add(-3, -2, -7));
		b.add(cl.clone().add(4, -2, -7));
		b.add(cl.clone().add(-4, -2, -7));
		b.add(cl.clone().add(5, -2, -7));
		b.add(cl.clone().add(-5, -2, -7));
		b.add(cl.clone().add(6, -2, -7));
		b.add(cl.clone().add(-6, -2, -7));

		// bL Edge
		b.add(cl.clone().add(-7, -2, -1));
		b.add(cl.clone().add(-7, -2, 0));
		b.add(cl.clone().add(-7, -2, 1));
		b.add(cl.clone().add(-7, -2, -2));
		b.add(cl.clone().add(-7, -2, 2));
		b.add(cl.clone().add(-7, -2, -3));
		b.add(cl.clone().add(-7, -2, 3));
		b.add(cl.clone().add(-7, -2, -4));
		b.add(cl.clone().add(-7, -2, 4));
		b.add(cl.clone().add(-7, -2, -5));
		b.add(cl.clone().add(-7, -2, 5));
		b.add(cl.clone().add(-7, -2, -6));
		b.add(cl.clone().add(-7, -2, 6));

		// bR Edge
		b.add(cl.clone().add(7, -2, -1));
		b.add(cl.clone().add(7, -2, 0));
		b.add(cl.clone().add(7, -2, 1));
		b.add(cl.clone().add(7, -2, -2));
		b.add(cl.clone().add(7, -2, 2));
		b.add(cl.clone().add(7, -2, -3));
		b.add(cl.clone().add(7, -2, 3));
		b.add(cl.clone().add(7, -2, -4));
		b.add(cl.clone().add(7, -2, 4));
		b.add(cl.clone().add(7, -2, -5));
		b.add(cl.clone().add(7, -2, 5));
		b.add(cl.clone().add(7, -2, -6));
		b.add(cl.clone().add(7, -2, 6));

		// tF Edge
		b.add(cl.clone().add(0, 12, 7));
		b.add(cl.clone().add(1, 12, 7));
		b.add(cl.clone().add(-1, 12, 7));
		b.add(cl.clone().add(2, 12, 7));
		b.add(cl.clone().add(-2, 12, 7));
		b.add(cl.clone().add(3, 12, 7));
		b.add(cl.clone().add(-3, 12, 7));
		b.add(cl.clone().add(4, 12, 7));
		b.add(cl.clone().add(-4, 12, 7));
		b.add(cl.clone().add(5, 12, 7));
		b.add(cl.clone().add(-5, 12, 7));
		b.add(cl.clone().add(6, 12, 7));
		b.add(cl.clone().add(-6, 12, 7));

		// tB Edge
		b.add(cl.clone().add(0, 12, -7));
		b.add(cl.clone().add(1, 12, -7));
		b.add(cl.clone().add(-1, 12, -7));
		b.add(cl.clone().add(2, 12, -7));
		b.add(cl.clone().add(-2, 12, -7));
		b.add(cl.clone().add(3, 12, -7));
		b.add(cl.clone().add(-3, 12, -7));
		b.add(cl.clone().add(4, 12, -7));
		b.add(cl.clone().add(-4, 12, -7));
		b.add(cl.clone().add(5, 12, -7));
		b.add(cl.clone().add(-5, 12, -7));
		b.add(cl.clone().add(6, 12, -7));
		b.add(cl.clone().add(-6, 12, -7));

		// tL Edge
		b.add(cl.clone().add(-7, 12, -1));
		b.add(cl.clone().add(-7, 12, 0));
		b.add(cl.clone().add(-7, 12, 1));
		b.add(cl.clone().add(-7, 12, -2));
		b.add(cl.clone().add(-7, 12, 2));
		b.add(cl.clone().add(-7, 12, 3));
		b.add(cl.clone().add(-7, 12, -3));
		b.add(cl.clone().add(-7, 12, 4));
		b.add(cl.clone().add(-7, 12, -4));
		b.add(cl.clone().add(-7, 12, 5));
		b.add(cl.clone().add(-7, 12, -5));
		b.add(cl.clone().add(-7, 12, 6));
		b.add(cl.clone().add(-7, 12, -6));

		// tR Edge
		b.add(cl.clone().add(7, 12, -1));
		b.add(cl.clone().add(7, 12, 0));
		b.add(cl.clone().add(7, 12, 1));
		b.add(cl.clone().add(7, 12, -2));
		b.add(cl.clone().add(7, 12, 2));
		b.add(cl.clone().add(7, 12, -3));
		b.add(cl.clone().add(7, 12, 3));
		b.add(cl.clone().add(7, 12, -3));
		b.add(cl.clone().add(7, 12, 4));
		b.add(cl.clone().add(7, 12, -4));
		b.add(cl.clone().add(7, 12, 5));
		b.add(cl.clone().add(7, 12, -5));
		b.add(cl.clone().add(7, 12, 6));
		b.add(cl.clone().add(7, 12, -6));

		// fL Edge
		b.add(cl.clone().add(7, 3, -7));
		b.add(cl.clone().add(7, 2, -7));
		b.add(cl.clone().add(7, 0, -7));
		b.add(cl.clone().add(7, 1, -7));
		b.add(cl.clone().add(7, -1, -7));
		b.add(cl.clone().add(7, 4, -7));
		b.add(cl.clone().add(7, 5, -7));
		b.add(cl.clone().add(7, 6, -7));
		b.add(cl.clone().add(7, 7, -7));
		b.add(cl.clone().add(7, 8, -7));
		b.add(cl.clone().add(7, 9, -7));
		b.add(cl.clone().add(7, 10, -7));
		b.add(cl.clone().add(7, 11, -7));

		// fR Edge
		b.add(cl.clone().add(7, 3, 7));
		b.add(cl.clone().add(7, 2, 7));
		b.add(cl.clone().add(7, 0, 7));
		b.add(cl.clone().add(7, 1, 7));
		b.add(cl.clone().add(7, -1, 7));
		b.add(cl.clone().add(7, 4, 7));
		b.add(cl.clone().add(7, 5, 7));
		b.add(cl.clone().add(7, 6, 7));
		b.add(cl.clone().add(7, 7, 7));
		b.add(cl.clone().add(7, 8, 7));
		b.add(cl.clone().add(7, 9, 7));
		b.add(cl.clone().add(7, 10, 7));
		b.add(cl.clone().add(7, 11, 7));

		// bL Edge
		b.add(cl.clone().add(-7, 3, -7));
		b.add(cl.clone().add(-7, 2, -7));
		b.add(cl.clone().add(-7, 0, -7));
		b.add(cl.clone().add(-7, 1, -7));
		b.add(cl.clone().add(-7, -1, -7));
		b.add(cl.clone().add(-7, 4, -7));
		b.add(cl.clone().add(-7, 5, -7));
		b.add(cl.clone().add(-7, 6, -7));
		b.add(cl.clone().add(-7, 7, -7));
		b.add(cl.clone().add(-7, 8, -7));
		b.add(cl.clone().add(-7, 9, -7));
		b.add(cl.clone().add(-7, 10, -7));
		b.add(cl.clone().add(-7, 11, -7));

		// bR Edge
		b.add(cl.clone().add(-7, 3, 7));
		b.add(cl.clone().add(-7, 2, 7));
		b.add(cl.clone().add(-7, 0, 7));
		b.add(cl.clone().add(-7, 1, 7));
		b.add(cl.clone().add(-7, -1, 7));
		b.add(cl.clone().add(-7, 4, 7));
		b.add(cl.clone().add(-7, 5, 7));
		b.add(cl.clone().add(-7, 6, 7));
		b.add(cl.clone().add(-7, 7, 7));
		b.add(cl.clone().add(-7, 8, 7));
		b.add(cl.clone().add(-7, 9, 7));
		b.add(cl.clone().add(-7, 10, 7));
		b.add(cl.clone().add(-7, 11, 7));

		// Bottom Corners
		b.add(cl.clone().add(7, -2, 7));
		b.add(cl.clone().add(-7, -2, 7));
		b.add(cl.clone().add(-7, -2, -7));
		b.add(cl.clone().add(7, -2, -7));

		// Top Corners
		b.add(cl.clone().add(7, 12, 7));
		b.add(cl.clone().add(-7, 12, 7));
		b.add(cl.clone().add(-7, 12, -7));
		b.add(cl.clone().add(7, 12, -7));

		for (final Location l : b) {
			new BukkitRunnable() {
				@Override
				public void run() {
					System.out.print(l.getBlock().getType().name());
					l.getBlock().setType(Material.AIR);
				}
			}.runTaskLater(CobbleCubes.getInstance(), 15);
		}
	}

	public Integer getMeta(ItemStack i, String key) {

		net.minecraft.server.v1_8_R3.ItemStack item = CraftItemStack.asNMSCopy(i);

		if (item.getTag() == null) {
			return 1;
		}

		return item.getTag().getInt(key);
	}
}
