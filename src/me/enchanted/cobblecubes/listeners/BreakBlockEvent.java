package me.enchanted.cobblecubes.listeners;

import org.bukkit.event.block.*;
import me.enchanted.cobblecubes.*;
import me.enchanted.cobblecubes.guis.LootGui;
import me.enchanted.cobblecubes.guis.UpgradesGui;
import me.enchanted.cobblecubes.utils.Glow;
import me.enchanted.cobblecubes.utils.ItemStacks;
import me.enchanted.cobblecubes.utils.PlayerData;
import me.enchanted.cobblecubes.utils.Randomize;
import net.brcdev.shopgui.ShopGuiPlusApi;

import org.bukkit.event.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

public class BreakBlockEvent implements Listener {

	public static HashMap<Location, ArrayList<ItemStack>> items = new HashMap<Location, ArrayList<ItemStack>>();

	@SuppressWarnings("serial")
	@EventHandler
	public void onBlockBreak(final BlockBreakEvent e) {
		final Block b = e.getBlock();
		PlayerData data = new PlayerData(e.getPlayer().getUniqueId());
		int m = 0;
		for (String keys : data.getConfig().getConfigurationSection("gens").getKeys(false)) {
			if (PlaceGeneratorEvent.center.get(e.getBlock().getLocation())
					.equals(new Location(Bukkit.getWorld(data.getConfig().getString("gens." + keys + ".center.world")),
							data.getConfig().getInt("gens." + keys + ".center.x"),
							data.getConfig().getInt("gens." + keys + ".center.y"),
							data.getConfig().getInt("gens." + keys + ".center.z")))) {

				m = getMaxQuantity(data.getConfig().getInt("gens." + keys + ".quantity"));
			}
		}
		final int max = m;
		if (CobbleCubes.getInstance().allGeneratorBlocks.get(e.getPlayer().getUniqueId())
				.contains(e.getBlock().getLocation())) {
			e.setCancelled(true);
			if (items.get(PlaceGeneratorEvent.center.get(e.getBlock().getLocation())) == null) {
				items.put(PlaceGeneratorEvent.center.get(e.getBlock().getLocation()), new ArrayList<ItemStack>() {
					{
						for (ItemStack is : b.getDrops()) {
							if (is.getType().equals(Material.IRON_ORE)) {
								is.setType(Material.IRON_INGOT);
							} else if (is.getType().equals(Material.GOLD_ORE)) {
								is.setType(Material.GOLD_INGOT);
							}
							int drops = 1;
							System.out.print(b.getType());
							if (b.getType().equals(Material.LAPIS_ORE)) {
								final Random randomizer = new Random();
								is.setDurability((short) 4);
								drops = randomizer.nextInt(8);
							} else if (b.getType().equals(Material.GLOWING_REDSTONE_ORE)
									|| b.getType().equals(Material.REDSTONE_ORE)) {
								final Random randomizer = new Random();
								drops = randomizer.nextInt(8);
							}
							if (e.getPlayer().getItemInHand().containsEnchantment(Enchantment.LOOT_BONUS_BLOCKS)) {
								Random r = new Random();
								int vF = 0;
								int level = e.getPlayer().getItemInHand()
										.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
								if (level <= 10) {
									vF = r.nextInt(3);
								} else if (level > 10 && level <= 20) {
									vF = r.nextInt(6);
								} else if (level > 20 && level <= 50) {
									vF = r.nextInt(12);
								} else if (level > 50 && level <= 75) {
									vF = r.nextInt(20);
								} else {
									vF = r.nextInt(50);
								}
								drops = drops * vF;
							}
							ItemMeta im = is.getItemMeta();
							if (b.getType().equals(Material.LAPIS_ORE)) {
								im.setDisplayName(
										"�6�l[!] �7" + StringUtils.capitalize("lapis_lazuli").replace("_", " "));
							} else {
								im.setDisplayName("�6�l[!] �7"
										+ StringUtils.capitalize(is.getType().name().toLowerCase()).replace("_", " "));
							}
							im.setLore(Arrays.asList(" �6�l* �e�lQuantity: �rx" + drops + " / " + max,
									" �6�l* �e�lWorth: �r$" + ShopGuiPlusApi.getItemStackPriceSell(e.getPlayer(), is),
									"�7�o(( Left Click for �r�bAll �7�o))", "�7�o(( Right Click to �r�aSell �7�o))"));
							is.setItemMeta(im);
							add(is);
							break;
						}
					}
				});
			} else {
				ArrayList<ItemStack> itemp = items.get(PlaceGeneratorEvent.center.get(e.getBlock().getLocation()));
				ArrayList<ItemStack> it = new ArrayList<ItemStack>();
				int count = 1;
				Material d = null;
				for (ItemStack d2 : b.getDrops()) {
					if (d2.getType().equals(Material.IRON_ORE)) {
						d2.setType(Material.IRON_INGOT);
					} else if (d2.getType().equals(Material.GOLD_ORE)) {
						d2.setType(Material.GOLD_INGOT);
					}
					d = d2.getType();
				}
				for (ItemStack k : itemp) {
					if (!k.getType().equals(d)) {
						it.add(k);
					} else {
						int drops = 0;

						for (ItemStack gA : b.getDrops()) {
							drops = gA.getAmount();
						}

						if (e.getPlayer().getItemInHand().containsEnchantment(Enchantment.LOOT_BONUS_BLOCKS)) {
							Random r = new Random();
							int vF = 0;
							int level = e.getPlayer().getItemInHand()
									.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
							if (level <= 10) {
								vF = r.nextInt(3);
							} else if (level > 10 && level <= 20) {
								vF = r.nextInt(6);
							} else if (level > 20 && level <= 50) {
								vF = r.nextInt(12);
							} else if (level > 50 && level <= 75) {
								vF = r.nextInt(20);
							} else {
								vF = r.nextInt(50);
							}
							drops = drops * vF;
						}

						if ((getQuantity(k) + drops) > max) {
							count = max;
						} else {
							count = getQuantity(k) + drops;
						}
					}
				}

				ItemStack is = new ItemStack(d);
				double price = ShopGuiPlusApi.getItemStackPriceSell(e.getPlayer(), is) * count;

				if (b.getType().equals(Material.LAPIS_ORE)) {
					final Random randomizer = new Random();
					is.setDurability((short) 4);
					count += randomizer.nextInt(8);
				} else if (b.getType().equals(Material.GLOWING_REDSTONE_ORE)
						|| b.getType().equals(Material.REDSTONE_ORE)) {
					final Random randomizer = new Random();
					count += randomizer.nextInt(8);
				}
				ItemMeta im = is.getItemMeta();

				if (b.getType().equals(Material.LAPIS_ORE)) {
					im.setDisplayName("�6�l[!] �7" + StringUtils.capitalize("lapis_lazuli").replace("_", " "));
				} else {
					im.setDisplayName(
							"�6�l[!] �7" + StringUtils.capitalize(is.getType().name().toLowerCase()).replace("_", " "));
				}
				im.setLore(
						Arrays.asList(" �6�l* �e�lQuantity: �rx" + count + " / " + max, " �6�l* �e�lWorth: �r$" + price,
								"�7�o(( Left Click for �r�bAll �7�o))", "�7�o(( Right Click to �r�aSell �7�o))"));
				is.setItemMeta(im);

				it.add(is);

				items.put(PlaceGeneratorEvent.center.get(e.getBlock().getLocation()), it);
			}
			b.setType(Material.AIR);

		}
	}

	@SuppressWarnings("serial")
	@EventHandler
	public void onTntEvent(final EntityExplodeEvent e) {
		for (final Block b : e.blockList()) {
			for (UUID u : CobbleCubes.getInstance().allGeneratorBlocks.keySet()) {
				if (CobbleCubes.getInstance().allGeneratorBlocks.get(u).contains(b.getLocation())) {
					e.setCancelled(true);

				}
			}
		}
	}

	@EventHandler
	public void onSell(final InventoryClickEvent e) {
		if (e.getInventory().getName().equalsIgnoreCase("�cCobble Cube")) {
			e.setCancelled(true);
			final Player p = (Player) e.getWhoClicked();
			PlayerData data = new PlayerData(p.getUniqueId());
			if (e.getSlot() == 48) {
				UpgradesGui.openInv(p, PlaceGeneratorEvent.pCenter.get(p));
			} else if (e.getSlot() == 53) {
				if (p.getInventory().firstEmpty() == -1) {
					p.sendMessage("�cYou inventory is full so you cannot remove this CobbleCube!");
					return;
				}
				int loot = 1;
				int quantity = 1;
				for (String keys : data.getConfig().getConfigurationSection("gens").getKeys(false)) {
					if (PlaceGeneratorEvent.pCenter.get(p)
							.equals(new Location(
									Bukkit.getWorld(data.getConfig().getString("gens." + keys + ".center.world")),
									data.getConfig().getInt("gens." + keys + ".center.x"),
									data.getConfig().getInt("gens." + keys + ".center.y"),
									data.getConfig().getInt("gens." + keys + ".center.z")))) {

						loot = data.getConfig().getInt("gens." + keys + ".loot");
						quantity = data.getConfig().getInt("gens." + keys + ".quantity");
					}
				}
				if (PlaceGeneratorEvent.type.get(PlaceGeneratorEvent.pCenter.get(p)).equalsIgnoreCase("3x3")) {
					p.getInventory().addItem(ItemStacks.getItemStack3X3(loot, quantity));
					PlaceGeneratorEvent.removeACube(p, PlaceGeneratorEvent.pCenter.get(p));
				} else if (PlaceGeneratorEvent.type.get(PlaceGeneratorEvent.pCenter.get(p)).equalsIgnoreCase("5x5")) {
					p.getInventory().addItem(ItemStacks.getItemStack5X5(loot, quantity));
					PlaceGeneratorEvent.removeBCube(p, PlaceGeneratorEvent.pCenter.get(p));
				} else if (PlaceGeneratorEvent.type.get(PlaceGeneratorEvent.pCenter.get(p)).equalsIgnoreCase("7x7")) {
					p.getInventory().addItem(ItemStacks.getItemStack7X7(loot, quantity));
					PlaceGeneratorEvent.removeCCube(p, PlaceGeneratorEvent.pCenter.get(p));
				} else if (PlaceGeneratorEvent.type.get(PlaceGeneratorEvent.pCenter.get(p)).equalsIgnoreCase("9x9")) {
					p.getInventory().addItem(ItemStacks.getItemStack9X9(loot, quantity));
					PlaceGeneratorEvent.removeDCube(p, PlaceGeneratorEvent.pCenter.get(p));
				} else if (PlaceGeneratorEvent.type.get(PlaceGeneratorEvent.pCenter.get(p)).equalsIgnoreCase("11x11")) {
					p.getInventory().addItem(ItemStacks.getItemStack11X11(loot, quantity));
					PlaceGeneratorEvent.removeECube(p, PlaceGeneratorEvent.pCenter.get(p));
				} else if (PlaceGeneratorEvent.type.get(PlaceGeneratorEvent.pCenter.get(p)).equalsIgnoreCase("13x13")) {
					p.getInventory().addItem(ItemStacks.getItemStack13X13(loot, quantity));
					PlaceGeneratorEvent.removeFCube(p, PlaceGeneratorEvent.pCenter.get(p));
				}
				p.playSound(p.getLocation(), Sound.EXPLODE, 1, 1);
			} else if (e.getSlot() == 50) {
				double price = 0;

				for (ItemStack is : items.get(PlaceGeneratorEvent.pCenter.get(p))) {
					price += ShopGuiPlusApi.getItemStackPriceSell((Player) e.getWhoClicked(), is) * getQuantity(is);
				}

				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
						"eco give " + e.getWhoClicked().getName() + " " + price);
				e.getWhoClicked().sendMessage("�aYou have sold all items for $" + price);
				items.get(PlaceGeneratorEvent.pCenter.get(p)).clear();
				openInv(p, PlaceGeneratorEvent.pCenter.get(p));

				p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
			} else if ((e.getSlot() >= 11 && e.getSlot() <= 15) || (e.getSlot() >= 20 && e.getSlot() <= 24)
					|| (e.getSlot() >= 29 && e.getSlot() <= 33)) {
				if (e.getClick().equals(ClickType.RIGHT)) {
					double price = ShopGuiPlusApi.getItemStackPriceSell((Player) e.getWhoClicked(), e.getCurrentItem());
					Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
							"eco give " + e.getWhoClicked().getName() + " " + price * getQuantity(e.getCurrentItem()));
					e.getWhoClicked()
							.sendMessage("�aYou have sold these items for $" + price * getQuantity(e.getCurrentItem()));
					items.get(PlaceGeneratorEvent.pCenter.get(p)).remove(e.getCurrentItem());
					e.setCurrentItem(new ItemStack(Material.AIR));
					openInv((Player) e.getWhoClicked(), PlaceGeneratorEvent.pCenter.get(p));
					p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
				} else {
					int toAdd = 0;
					int quantity = getQuantity(e.getCurrentItem());
					System.out.print(quantity);
					System.out.print("h");
					int empty = getEmptySlots(p);
					empty = empty * 64;
					for (int i = 0; i < quantity; i++) {
						System.out.print(quantity);
						if (p.getInventory().firstEmpty() == -1 || quantity > empty) {
							toAdd = empty;
							System.out.print(toAdd);
							System.out.print(empty);
							break;
						} else {
							toAdd++;
						}
					}

					if (p.getInventory().firstEmpty() != -1) {
						for (int i = 0; i < toAdd; i++) {
							ItemStack is = new ItemStack(e.getCurrentItem().getType());
							p.getInventory().addItem(is);
						}

						final int newQuantity = quantity - toAdd;
						System.out.print(newQuantity);
						System.out.print(quantity);
						System.out.print(toAdd);

						p.playSound(p.getLocation(), Sound.CLICK, 1, 1);
						if (newQuantity == 0) {
							items.get(PlaceGeneratorEvent.pCenter.get(p)).remove(e.getCurrentItem());
							e.setCurrentItem(new ItemStack(Material.AIR));

							openInv(p, PlaceGeneratorEvent.pCenter.get(p));
						} else {
							items.get(PlaceGeneratorEvent.pCenter.get(p)).remove(e.getCurrentItem());
							items.put(PlaceGeneratorEvent.pCenter.get(p), new ArrayList<ItemStack>() {
								{
									for (ItemStack is : items.get(PlaceGeneratorEvent.pCenter.get(p))) {
										add(is);
									}
									ItemMeta im = e.getCurrentItem().getItemMeta();

									if (e.getCurrentItem().isSimilar(new ItemStack(Material.INK_SACK, 1, (short) 4))) {
										im.setDisplayName("�6�l[!] �7"
												+ StringUtils.capitalize("lapis_lazuli").replace("_", " "));
									} else {
										im.setDisplayName("�6�l[!] �7" + StringUtils
												.capitalize(e.getCurrentItem().getType().name().toLowerCase())
												.replace("_", " "));
									}
									im.setLore(Arrays.asList(
											" �6�l* �e�lQuantity: �rx" + newQuantity + " / " + getMaxQuantity(quantity),
											" �6�l* �e�lWorth: �r$"
													+ ShopGuiPlusApi.getItemStackPriceSell(p, e.getCurrentItem())
															* newQuantity,
											"�7�o(( Left Click for �r�bAll �7�o))",
											"�7�o(( Right Click to �r�aSell �7�o))"));
									e.getCurrentItem().setItemMeta(im);
									add(e.getCurrentItem());
								}
							});
							openInv((Player) e.getWhoClicked(), PlaceGeneratorEvent.pCenter.get(p));
						}
					} else {
						p.sendMessage("You do not have enough inventory space!");
					}
				}
			}
		}
	}

	public static void openInv(Player p, Location l) {
		PlayerData data = new PlayerData(p.getUniqueId());
		int max = 0;
		for (String keys : data.getConfig().getConfigurationSection("gens").getKeys(false)) {
			if (l.equals(new Location(Bukkit.getWorld(data.getConfig().getString("gens." + keys + ".center.world")),
					data.getConfig().getInt("gens." + keys + ".center.x"),
					data.getConfig().getInt("gens." + keys + ".center.y"),
					data.getConfig().getInt("gens." + keys + ".center.z")))) {

				max = getMaxQuantity(data.getConfig().getInt("gens." + keys + ".quantity"));
			}
		}

		Inventory inv = Bukkit.createInventory(null, 54, "�cCobble Cube");
		for (int i = 0; i < inv.getSize(); i++) {
			ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 7);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("�7");
			is.setItemMeta(im);
			inv.setItem(i, is);
		}
		for (int i = 11; i <= 15; i++) {
			inv.setItem(i, null);
		}
		for (int i = 20; i <= 24; i++) {
			inv.setItem(i, null);
		}
		for (int i = 29; i <= 33; i++) {
			inv.setItem(i, null);
		}
		for (int i = 21; i < items.size(); i++) {
			items.get(l).remove(i);
		}

		int y = 0;
		int g = 11;
		for (ItemStack is : items.get(l)) {
			if (y == 5) {
				y = 0;
				g += 4;
			}
			ItemMeta im = is.getItemMeta();
			im.setLore(Arrays.asList(" �6�l* �e�lQuantity: �rx" + getQuantity(is) + " / " + max,
					" �6�l* �e�lWorth: �r$" + ShopGuiPlusApi.getItemStackPriceSell(p, is) * getQuantity(is),
					"�7�o(( Left Click for �r�bAll �7�o))", "�7�o(( Right Click to �r�aSell �7�o))"));
			is.setItemMeta(im);
			inv.setItem(g, is);
			g++;
			y++;
		}

		ItemStack up = new ItemStack(Material.HOPPER);
		ItemMeta um = up.getItemMeta();
		um.setDisplayName("�5�l[!] �d�lUPGRADE COBBLECUBE �7(Click)");
		um.setLore(Arrays.asList("�7", "�7The �dUpgrade Menu �7allows you to upgrade",
				"�7your �5speed, storage �7and �5ores�7."));
		up.setItemMeta(um);

		Double totalPrice = 0.00;
		DecimalFormat df = new DecimalFormat("#.##");
		for (ItemStack i : items.get(PlaceGeneratorEvent.pCenter.get(p))) {
			totalPrice += ShopGuiPlusApi.getItemStackPriceSell(p, i) * getQuantity(i);
		}

		ItemStack sell = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		SkullMeta sim = (SkullMeta) sell.getItemMeta();
		sim.setDisplayName("�2�l[!] �a�lSELL ALL �7(Click)");
		sim.setOwner("Trajan");
		sim.setLore(Arrays.asList("�7", "�7Once clicked, your �2items will disappear�7,",
				"�7and you will be given �athe money earned�7!", "�7",
				" �2�l* �a�lCURRENT VALUE�7: �r$" + df.format(totalPrice)));
		sell.setItemMeta(sim);

		ItemStack rc = new ItemStack(Material.BARRIER);
		ItemMeta rcim = rc.getItemMeta();
		rcim.setDisplayName("�4�l[!] �c�lREMOVE COBBLECUBE �7(Click)");
		rcim.setLore(Arrays.asList("�7", "�7Once clicked, your �cCobbleCube will despawn�7,",
				"�7and you will be place �ainto your inventory�7.", "�7",
				" �c�oWARNING: �f�oYou will lose your items inside if you remove!"));
		rc.setItemMeta(rcim);

		inv.setItem(48, up);
		inv.setItem(50, sell);
		inv.setItem(53, rc);

		p.openInventory(inv);
	}

	public int getEmptySlots(Player p) {
		PlayerInventory inventory = p.getInventory();
		ItemStack[] cont = inventory.getContents();
		int i = 0;
		for (ItemStack item : cont)
			if (item != null && item.getType() != Material.AIR) {
				i++;
			}
		return 36 - i;
	}

	public static Integer getQuantity(ItemStack is) {
		if (is != null && is.hasItemMeta() && is.getItemMeta().hasLore()) {
			for (String l : is.getItemMeta().getLore()) {
				String[] split = l.split(" ");
				System.out.print(split[2]);
				if (split[2].equalsIgnoreCase("�e�lQuantity:")) {
					int i = Integer.parseInt(split[3].replaceAll("[^\\d.]", ""));
					return i;
				}
			}
		}
		return null;
	}

	public static Integer getMaxQuantity(int level) {
		return CobbleCubes.getInstance().getConfig().getInt("storage." + level);
	}
}
