package me.enchanted.cobblecubes.utils;

import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import me.enchanted.cobblecubes.*;
import java.util.*;

@SuppressWarnings("deprecation")
public class Randomize {

	public static Material getRandomBlock() {
		final FileConfiguration config = CobbleCubes.getInstance().getConfig();
		final Random randomizer = new Random();
		final List<Integer> id = new ArrayList<Integer>();
		for (final String key : config.getConfigurationSection("spawningblocks").getKeys(false)) {
			for (int i = 0; i < config.getInt("spawningblocks." + key + ".chance"); ++i) {
				id.add(config.getInt("spawningblocks." + key + ".id"));
			}
		}
		final int index = randomizer.nextInt(id.size());
		return Material.getMaterial((int) id.get(index));
	}

	public static List<Material> get27RandomBlocks(int loot) {
		final FileConfiguration config = CobbleCubes.getInstance().getConfig();
		final List<Material> blocks = new ArrayList<Material>();
		final Random randomizer = new Random();
		final List<String> id = new ArrayList<String>();
		for (final String ore : config.getConfigurationSection("loot." + loot).getKeys(false)) {
			for (int i = 0; i < config.getInt("loot." + loot + "." + ore); ++i) {
				id.add(ore);
			}
		}
		for (int ii = 0; ii < 27; ++ii) {
			final int index = randomizer.nextInt(id.size());
			blocks.add(Material.getMaterial(id.get(index).toUpperCase()));
		}
		return blocks;
	}

	public static List<Material> get125RandomBlocks(int loot) {
		final FileConfiguration config = CobbleCubes.getInstance().getConfig();
		final List<Material> blocks = new ArrayList<Material>();
		final Random randomizer = new Random();
		final List<String> id = new ArrayList<String>();
		for (final String ore : config.getConfigurationSection("loot." + loot).getKeys(false)) {
			for (int i = 0; i < config.getInt("loot." + loot + "." + ore); ++i) {
				id.add(ore);
			}
		}
		for (int ii = 0; ii < 125; ++ii) {
			final int index = randomizer.nextInt(id.size());
			blocks.add(Material.getMaterial(id.get(index).toUpperCase()));
		}
		return blocks;
	}

	public static List<Material> get343RandomBlocks(int loot) {
		final FileConfiguration config = CobbleCubes.getInstance().getConfig();
		final List<Material> blocks = new ArrayList<Material>();
		final Random randomizer = new Random();
		final List<String> id = new ArrayList<String>();
		for (final String ore : config.getConfigurationSection("loot." + loot).getKeys(false)) {
			for (int i = 0; i < config.getInt("loot." + loot + "." + ore); ++i) {
				id.add(ore);
			}
		}
		for (int ii = 0; ii < 343; ++ii) {
			final int index = randomizer.nextInt(id.size());
			blocks.add(Material.getMaterial(id.get(index).toUpperCase()));
		}
		return blocks;
	}

	public static List<Material> get729RandomBlocks(int loot) {
		final FileConfiguration config = CobbleCubes.getInstance().getConfig();
		final List<Material> blocks = new ArrayList<Material>();
		final Random randomizer = new Random();
		final List<String> id = new ArrayList<String>();
		for (final String ore : config.getConfigurationSection("loot." + loot).getKeys(false)) {
			for (int i = 0; i < config.getInt("loot." + loot + "." + ore); ++i) {
				id.add(ore);
			}
		}
		for (int ii = 0; ii < 729; ++ii) {
			final int index = randomizer.nextInt(id.size());
			blocks.add(Material.getMaterial(id.get(index).toUpperCase()));
		}
		return blocks;
	}

	public static List<Material> get1331RandomBlocks(int loot) {
		final FileConfiguration config = CobbleCubes.getInstance().getConfig();
		final List<Material> blocks = new ArrayList<Material>();
		final Random randomizer = new Random();
		final List<String> id = new ArrayList<String>();
		for (final String ore : config.getConfigurationSection("loot." + loot).getKeys(false)) {
			for (int i = 0; i < config.getInt("loot." + loot + "." + ore); ++i) {
				id.add(ore);
			}
		}
		for (int ii = 0; ii < 1331; ++ii) {
			final int index = randomizer.nextInt(id.size());
			blocks.add(Material.getMaterial(id.get(index).toUpperCase()));
		}
		return blocks;
	}

	public static List<Material> get2197RandomBlocks(int loot) {
		final FileConfiguration config = CobbleCubes.getInstance().getConfig();
		final List<Material> blocks = new ArrayList<Material>();
		final Random randomizer = new Random();
		final List<String> id = new ArrayList<String>();
		for (final String ore : config.getConfigurationSection("loot." + loot).getKeys(false)) {
			for (int i = 0; i < config.getInt("loot." + loot + "." + ore); ++i) {
				id.add(ore);
			}
		}
		for (int ii = 0; ii < 2197; ++ii) {
			final int index = randomizer.nextInt(id.size());
			blocks.add(Material.getMaterial(id.get(index).toUpperCase()));
		}
		return blocks;
	}

	public static List<Material> get3375RandomBlocks(int loot) {
		final FileConfiguration config = CobbleCubes.getInstance().getConfig();
		final List<Material> blocks = new ArrayList<Material>();
		final Random randomizer = new Random();
		final List<String> id = new ArrayList<String>();
		for (final String ore : config.getConfigurationSection("loot." + loot).getKeys(false)) {
			for (int i = 0; i < config.getInt("loot." + loot + "." + ore); ++i) {
				id.add(ore);
			}
		}
		for (int ii = 0; ii < 3375; ++ii) {
			final int index = randomizer.nextInt(id.size());
			blocks.add(Material.getMaterial(id.get(index).toUpperCase()));
		}
		return blocks;
	}
}
