package me.enchanted.cobblecubes.utils;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import me.enchanted.cobblecubes.CobbleCubes;

public enum Messages {

	NOPERMMISION("messages.nopermission"), 
    NOARGUMENTS("messages.noarguments"),
	TOCLOSETOPLACE("messages.toclosetoplace"),
	NOTENOUGHSPACE("messages.notenoughspacetoplace"),
	SUCCESSFULLYPLACED("messages.successfully_placed");

	private String path;

	private Messages(final String path) {
		this.path = path;
	}

	public void send(final Player p) {
		for (final String line : CobbleCubes.getInstance().getConfig().getString(this.path).split("\\n")) {
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', line));
		}
	}
}
