package me.enchanted.cobblecubes.utils;

import org.bukkit.inventory.*;

import java.util.Arrays;

import org.bukkit.*;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.meta.*;

import net.minecraft.server.v1_8_R3.NBTTagCompound;

public class ItemStacks {
	
	public static ItemStack getItemStack3X3() {
		ItemStack item = new ItemStack(Material.COAL_ORE);
		final ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName("�4�l3x3 Cobble Cube");
		item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack getItemStack3X3(int loot, int quantity) {
		ItemStack item = new ItemStack(Material.COAL_ORE);
		item = setMeta(item, quantity, loot);
		final ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName("�e�lCOBBLE�6�lCUBE �7(Place Down)");
		itemm.setLore(Arrays.asList("�7�o\"This CobbleCube will automatically generate", 
				"�7�oore depending on the upgrades.\"",
				" �6�l* �e�lSize: �r�l3x3",
				" �6�l* �e�lUpgrades:",
				"   �r- �eStorage �7" + quantity,
				"   �r- �eLoot �7" + loot,
				"�7",
				"�7�o(( �r�oPlace Down �7�othis CobbleCube to begin generating ores ))"));
		item.setItemMeta(itemm);
		return item;
	}

	public static ItemStack getItemStack5X5(int loot, int quantity) {
		ItemStack item = new ItemStack(Material.IRON_ORE);
		item = setMeta(item, quantity, loot);
		final ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName("�e�lCOBBLE�6�lCUBE �7(Place Down)");
		itemm.setLore(Arrays.asList("�7�o\"This CobbleCube will automatically generate", 
				"�7�oore depending on the upgrades.\"",
				" �6�l* �e�lSize: �r�l5x5",
				" �6�l* �e�lUpgrades:",
				"   �r- �eStorage �7" + quantity,
				"   �r- �eLoot �7" + loot,
				"�7",
				"�7�o(( �r�oPlace Down �7�othis CobbleCube to begin generating ores ))"));
		item.setItemMeta(itemm);
		return item;
	}

	public static ItemStack getItemStack7X7(int loot, int quantity) {
		ItemStack item = new ItemStack(Material.LAPIS_ORE);
		item = setMeta(item, quantity, loot);
		final ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName("�e�lCOBBLE�6�lCUBE �7(Place Down)");
		itemm.setLore(Arrays.asList("�7�o\"This CobbleCube will automatically generate", 
				"�7�oore depending on the upgrades.\"",
				" �6�l* �e�lSize: �r�l7x7",
				" �6�l* �e�lUpgrades:",
				"   �r- �eStorage �7" + quantity,
				"   �r- �eLoot �7" + loot,
				"�7",
				"�7�o(( �r�oPlace Down �7�othis CobbleCube to begin generating ores ))"));
		item.setItemMeta(itemm);
		return item;
	}

	public static ItemStack getItemStack9X9(int loot, int quantity) {
		ItemStack item = new ItemStack(Material.REDSTONE_ORE);
		item = setMeta(item, quantity, loot);
		final ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName("�e�lCOBBLE�6�lCUBE �7(Place Down)");
		itemm.setLore(Arrays.asList("�7�o\"This CobbleCube will automatically generate", 
				"�7�oore depending on the upgrades.\"",
				" �6�l* �e�lSize: �r�l9x9",
				" �6�l* �e�lUpgrades:",
				"   �r- �eStorage �7" + quantity,
				"   �r- �eLoot �7" + loot,
				"�7",
				"�7�o(( �r�oPlace Down �7�othis CobbleCube to begin generating ores ))"));
		item.setItemMeta(itemm);
		return item;
	}

	public static ItemStack getItemStack11X11(int loot, int quantity) {
		ItemStack item = new ItemStack(Material.GOLD_ORE);
		item = setMeta(item, quantity, loot);
		final ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName("�e�lCOBBLE�6�lCUBE �7(Place Down)");
		itemm.setLore(Arrays.asList("�7�o\"This CobbleCube will automatically generate", 
				"�7�oore depending on the upgrades.\"",
				" �6�l* �e�lSize: �r�l11x11",
				" �6�l* �e�lUpgrades:",
				"   �r- �eStorage �7" + quantity,
				"   �r- �eLoot �7" + loot,
				"�7",
				"�7�o(( �r�oPlace Down �7�othis CobbleCube to begin generating ores ))"));
		item.setItemMeta(itemm);
		return item;
	}

	public static ItemStack getItemStack13X13(int loot, int quantity) {
		ItemStack item = new ItemStack(Material.DIAMOND_ORE);
		item = setMeta(item, quantity, loot);
		final ItemMeta itemm = item.getItemMeta();
		itemm.setDisplayName("�e�lCOBBLE�6�lCUBE �7(Place Down)");
		itemm.setLore(Arrays.asList("�7�o\"This CobbleCube will automatically generate", 
				"�7�oore depending on the upgrades.\"",
				" �6�l* �e�lSize: �r�l13x13",
				" �6�l* �e�lUpgrades:",
				"   �r- �eStorage �7" + quantity,
				"   �r- �eLoot �7" + loot,
				"�7",
				"�7�o(( �r�oPlace Down �7�othis CobbleCube to begin generating ores ))"));
		item.setItemMeta(itemm);
		return item;
	}

	public static ItemStack setMeta(ItemStack i, int quantity, int loot) {

		net.minecraft.server.v1_8_R3.ItemStack Itemstack = CraftItemStack.asNMSCopy(i);

		NBTTagCompound compound = new NBTTagCompound();

		compound.setInt("quantity", quantity);
		compound.setInt("loot", loot);

		Itemstack.setTag(compound);

		i = CraftItemStack.asBukkitCopy(Itemstack);

		return i;
	}
}
