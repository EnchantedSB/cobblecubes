package me.enchanted.cobblecubes.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.enchanted.cobblecubes.CobbleCubes;
import me.enchanted.cobblecubes.listeners.PlaceGeneratorEvent;

public class PlayerData {

	private File file;
	public FileConfiguration config;

	public PlayerData(UUID name) {
		file = new File(CobbleCubes.getInstance().getDataFolder() + "/CubeData", name.toString() + ".yml");
		config = YamlConfiguration.loadConfiguration(file);
	}

	public boolean exists() {
		return file.exists();
	}

	public Configuration getConfig() {
		return config;
	}

	public void createGenerator(String type, Location center, int quantity, int loot, int multiplier) {
		try {
			UUID rU = UUID.randomUUID();
			config.set("gens." + rU + ".type", type);
			config.set("gens." + rU + ".center.world", center.getWorld().getName());
			config.set("gens." + rU + ".center.x", center.getX());
			config.set("gens." + rU + ".center.y", center.getY());
			config.set("gens." + rU + ".center.z", center.getZ());
			config.set("gens." + rU + ".respawn-speed-in-ticks", 150);
			config.set("gens." + rU + ".quantity", quantity);
			config.set("gens." + rU + ".loot", loot);
			config.set("gens." + rU + ".multiplier", multiplier);
			config.save(file);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void removeGenerator(Location center) {
		try {
			for (String keys : getConfig().getConfigurationSection("gens").getKeys(false)) {
				if (center.equals(new Location(Bukkit.getWorld(getConfig().getString("gens." + keys + ".center.world")),
						getConfig().getInt("gens." + keys + ".center.x"),
						getConfig().getInt("gens." + keys + ".center.y"),
						getConfig().getInt("gens." + keys + ".center.z")))) {
					getConfig().set("gens." + keys, null);
					config.save(file);

				}
			}
			config.save(file);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void upgradeQuantity(Location center) throws IOException {
		for (String keys : getConfig().getConfigurationSection("gens").getKeys(false)) {
			if (center.equals(new Location(Bukkit.getWorld(getConfig().getString("gens." + keys + ".center.world")),
					getConfig().getInt("gens." + keys + ".center.x"), getConfig().getInt("gens." + keys + ".center.y"),
					getConfig().getInt("gens." + keys + ".center.z")))) {
				getConfig().set("gens." + keys + ".quantity", getConfig().getInt("gens." + keys + ".quantity") + 1);
				config.save(file);

			}
		}
	}

	public void setLoot(Location center, int level) throws IOException {
		for (String keys : getConfig().getConfigurationSection("gens").getKeys(false)) {
			if (center.equals(new Location(Bukkit.getWorld(getConfig().getString("gens." + keys + ".center.world")),
					getConfig().getInt("gens." + keys + ".center.x"), getConfig().getInt("gens." + keys + ".center.y"),
					getConfig().getInt("gens." + keys + ".center.z")))) {
				getConfig().set("gens." + keys + ".loot", level);
				config.save(file);

			}
		}
	}

	public void setMultiplier(Location center, int level) throws IOException {
		for (String keys : getConfig().getConfigurationSection("gens").getKeys(false)) {
			if (center.equals(new Location(Bukkit.getWorld(getConfig().getString("gens." + keys + ".center.world")),
					getConfig().getInt("gens." + keys + ".center.x"), getConfig().getInt("gens." + keys + ".center.y"),
					getConfig().getInt("gens." + keys + ".center.z")))) {
				getConfig().set("gens." + keys + ".multiplier", level);
				config.save(file);

			}
		}
	}

	@SuppressWarnings("deprecation")
	public static List<Material> get27RandomBlocks(int level) {
		final FileConfiguration config = CobbleCubes.getInstance().getConfig();
		final List<Material> blocks = new ArrayList<Material>();
		final Random randomizer = new Random();
		final List<Integer> id = new ArrayList<Integer>();
		for (final String key : config.getConfigurationSection("spawningblocks").getKeys(false)) {
			for (int i = 0; i < config.getInt("spawningblocks." + key + ".chance"); ++i) {
				id.add(config.getInt("spawningblocks." + key + ".id"));
			}
		}
		for (int ii = 0; ii < 27; ++ii) {
			final int index = randomizer.nextInt(id.size());
			blocks.add(Material.getMaterial((int) id.get(index)));
		}
		return blocks;
	}

	@SuppressWarnings({ "deprecation", "unused" })
	public static Material getRandomBlock(int level) {
		final FileConfiguration config = CobbleCubes.getInstance().getConfig();
		final Random randomizer = new Random();
		final List<String> blocks = new ArrayList<String>();
		for (final String key : config.getConfigurationSection("loot").getKeys(false)) {
			if (level == Integer.parseInt(key)) {
				for (final String block : config.getConfigurationSection("loot." + key).getKeys(false)) {
					for (int i = 0; i < config.getInt("loot." + key + "." + block); ++i) {
						blocks.add(block);
					}
				}
			}
		}
		final int index = randomizer.nextInt(blocks.size());
		return Material.getMaterial(blocks.get(index).toUpperCase());
	}
}