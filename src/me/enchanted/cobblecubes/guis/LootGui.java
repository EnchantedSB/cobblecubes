package me.enchanted.cobblecubes.guis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import me.enchanted.cobblecubes.CobbleCubes;
import me.enchanted.cobblecubes.listeners.PlaceGeneratorEvent;
import me.enchanted.cobblecubes.utils.Glow;
import me.enchanted.cobblecubes.utils.PlayerData;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;

public class LootGui implements Listener {

	public static void openInv(Player p, Location center) {
		Inventory inv = Bukkit.createInventory(null, 54, "§a§lUpgrade Loot");

		for (int i = 0; i < inv.getSize(); i++) {
			ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 15);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("§7");
			is.setItemMeta(im);

			inv.setItem(i, is);
		}

		inv.setItem(0, getItem(2, Material.QUARTZ, p, center));
		inv.setItem(9, getItem(3, Material.QUARTZ, p, center));
		inv.setItem(18, getItem(4, Material.QUARTZ, p, center));
		inv.setItem(27, getItem(5, Material.QUARTZ, p, center));
		inv.setItem(36, getItem(6, Material.EMERALD, p, center));
		inv.setItem(37, getItem(7, Material.QUARTZ, p, center));
		inv.setItem(38, getItem(8, Material.QUARTZ, p, center));
		inv.setItem(29, getItem(9, Material.QUARTZ, p, center));
		inv.setItem(20, getItem(10, Material.QUARTZ, p, center));
		inv.setItem(11, getItem(11, Material.EMERALD, p, center));
		inv.setItem(2, getItem(12, Material.QUARTZ, p, center));
		inv.setItem(3, getItem(13, Material.QUARTZ, p, center));
		inv.setItem(4, getItem(14, Material.QUARTZ, p, center));
		inv.setItem(13, getItem(15, Material.QUARTZ, p, center));
		inv.setItem(22, getItem(16, Material.EMERALD, p, center));
		inv.setItem(31, getItem(17, Material.QUARTZ, p, center));
		inv.setItem(40, getItem(18, Material.QUARTZ, p, center));
		inv.setItem(41, getItem(19, Material.QUARTZ, p, center));
		inv.setItem(42, getItem(20, Material.QUARTZ, p, center));
		inv.setItem(33, getItem(21, Material.EMERALD, p, center));
		inv.setItem(24, getItem(22, Material.QUARTZ, p, center));
		inv.setItem(15, getItem(23, Material.QUARTZ, p, center));
		inv.setItem(6, getItem(24, Material.QUARTZ, p, center));
		inv.setItem(7, getItem(25, Material.QUARTZ, p, center));
		inv.setItem(8, getItem(26, Material.EMERALD, p, center));
		inv.setItem(17, getItem(27, Material.QUARTZ, p, center));
		inv.setItem(26, getItem(28, Material.QUARTZ, p, center));
		inv.setItem(35, getItem(29, Material.QUARTZ, p, center));
		inv.setItem(44, getItem(30, Material.EMERALD_BLOCK, p, center));

		p.openInventory(inv);
	}

	public static ItemStack getItem(int level, Material mat, Player p, Location center) {

		ArrayList<String> l = new ArrayList<String>();
		String oloot = "";
		String loot = "";

		for (String lo : CobbleCubes.getInstance().getConfig().getConfigurationSection("loot").getKeys(false)) {
			if (Integer.parseInt(lo) == level) {
				for (String ores : CobbleCubes.getInstance().getConfig().getConfigurationSection("loot." + lo)
						.getKeys(false)) {
					int percent = CobbleCubes.getInstance().getConfig().getInt("loot." + lo + "." + ores);
					if (ores.equalsIgnoreCase("cobblestone")) {
						loot += "§7■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("coal_ore")) {
						loot += "§8■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("iron_ore")) {
						loot += "§e■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("lapis_ore")) {
						loot += "§9■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("redstone_ore")) {
						loot += "§4■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("gold_ore")) {
						loot += "§6■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("diamond_ore")) {
						loot += "§b■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("emerald_ore")) {
						loot += "§a■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("coal_block")) {
						loot += "§8⬛ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("iron_block")) {
						loot += "§e⬛ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("lapis_block")) {
						loot += "§9⬛ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("redstone_block")) {
						loot += "§4⬛ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("gold_block")) {
						loot += "§6⬛ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("diamond_block")) {
						loot += "§b⬛ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("emerald_block")) {
						loot += "§a⬛ §r" + percent + "%  ";
					}
				}
			}
			if (Integer.parseInt(lo) == (level - 1)) {
				for (String ores : CobbleCubes.getInstance().getConfig().getConfigurationSection("loot." + lo)
						.getKeys(false)) {
					int percent = CobbleCubes.getInstance().getConfig().getInt("loot." + lo + "." + ores);
					if (ores.equalsIgnoreCase("cobblestone")) {
						oloot += "§7■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("coal_ore")) {
						oloot += "§8■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("iron_ore")) {
						oloot += "§e■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("lapis_ore")) {
						oloot += "§9■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("redstone_ore")) {
						oloot += "§4■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("gold_ore")) {
						oloot += "§6■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("diamond_ore")) {
						oloot += "§b■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("emerald_ore")) {
						oloot += "§a■ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("coal_block")) {
						oloot += "§8⬛ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("iron_block")) {
						oloot += "§e⬛ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("lapis_block")) {
						oloot += "§9⬛ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("redstone_block")) {
						oloot += "§4⬛ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("gold_block")) {
						oloot += "§6⬛ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("diamond_block")) {
						oloot += "§b⬛ §r" + percent + "%  ";
					} else if (ores.equalsIgnoreCase("emerald_block")) {
						oloot += "§a⬛ §r" + percent + "%  ";
					}
				}
			}
		}
		
		PlayerData data = new PlayerData(p.getUniqueId());
		int original = 0;
		ItemStack is = null;
		for (String keys : data.getConfig().getConfigurationSection("gens").getKeys(false)) {
			if (center
					.equals(new Location(Bukkit.getWorld(data.getConfig().getString("gens." + keys + ".center.world")),
							data.getConfig().getInt("gens." + keys + ".center.x"),
							data.getConfig().getInt("gens." + keys + ".center.y"),
							data.getConfig().getInt("gens." + keys + ".center.z")))) {
				original = data.getConfig().getInt("gens." + keys + ".loot");
			}
		}

		if(level > original+1) {
			System.out.print(level);
			System.out.print(original+1);
			is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 14);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("§c§l§o???");
			is.setItemMeta(im);
		} else if(level < original+1) {
			System.out.print(level);
			System.out.print(original+1);
			is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 5);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("§a§lPURCHASED");
			is.setItemMeta(im);
		} else {
			is = new ItemStack(mat);
			
			l.add("§7");
			l.add(" §2§l* §a§lLevel: §c" + (level - 1) + " §r--> §a" + level);
			l.add(" §c- " + oloot);
			l.add(" §a+ " + loot);
			l.add(" §2§l* §a§lCost: §a$"
					+ String.format("%,d", CobbleCubes.getInstance().getConfig().getInt("cost." + PlaceGeneratorEvent.type.get(PlaceGeneratorEvent.pCenter.get(p)) + "loot." + level)));

			
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("§2§l[!] §a§lLEVEL " + level);
			im.setLore(l);
			is.setItemMeta(im);
		}
		
		for (String keys : data.getConfig().getConfigurationSection("gens").getKeys(false)) {
			if (center
					.equals(new Location(Bukkit.getWorld(data.getConfig().getString("gens." + keys + ".center.world")),
							data.getConfig().getInt("gens." + keys + ".center.x"),
							data.getConfig().getInt("gens." + keys + ".center.y"),
							data.getConfig().getInt("gens." + keys + ".center.z")))) {
				if (level == data.getConfig().getInt("gens." + keys + ".loot") + 1) {
					is.addUnsafeEnchantment(new Glow(120), 1);
				}
			}
		}

		return is;
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if (e.getInventory().getName().equalsIgnoreCase("§a§lUpgrade Loot")) {
			e.setCancelled(true);
			Player p = (Player) e.getWhoClicked();
			if (e.getCurrentItem().containsEnchantment(new Glow(120))) {
				int level = getLevel(e.getCurrentItem());

				EconomyResponse r = CobbleCubes.econ.withdrawPlayer((OfflinePlayer) e.getWhoClicked(),
						CobbleCubes.getInstance().getConfig().getInt("cost." + PlaceGeneratorEvent.type.get(PlaceGeneratorEvent.pCenter.get(e.getWhoClicked())) +"loot." + level));
				if (r.transactionSuccess()) {
					e.getWhoClicked().sendMessage("§aYou have successfully bought Level " + level + " on loot!");

					Location center = PlaceGeneratorEvent.pCenter.get((Player)e.getWhoClicked());

					PlayerData data = new PlayerData(e.getWhoClicked().getUniqueId());

					for (String keys : data.getConfig().getConfigurationSection("gens").getKeys(false)) {
						if (center.equals(new Location(
								Bukkit.getWorld(data.getConfig().getString("gens." + keys + ".center.world")),
								data.getConfig().getInt("gens." + keys + ".center.x"),
								data.getConfig().getInt("gens." + keys + ".center.y"),
								data.getConfig().getInt("gens." + keys + ".center.z")))) {

							p.playSound(p.getLocation(), Sound.ANVIL_USE, 1, 1);
							ItemStack is = new ItemStack(Material.PAPER);
							ItemMeta im = is.getItemMeta();
							im.setDisplayName("§c§lUPGRADING...");
							is.setItemMeta(im);
							e.setCurrentItem(is);
							e.getInventory().setItem(e.getSlot(), is);
							new BukkitRunnable() {

								@Override
								public void run() {

									p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
									try {
										data.setLoot(center, level);
										openInv((Player) e.getWhoClicked(), center);
									} catch (IOException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								}
							}.runTaskLater(CobbleCubes.getInstance(), 20);

						}
					}
				} else {
					e.getWhoClicked().sendMessage("§c§lYou do not have enough to purchase this!");
				}
			} else {
				e.getWhoClicked().sendMessage("§cYou cannot purchase that.");
			}
		}
	}

	public static Integer getLevel(ItemStack is) {
		if (is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName()) {
			String l = is.getItemMeta().getDisplayName();
			String[] split = l.split(" ");
			if (split[1].equalsIgnoreCase("§a§lLEVEL")) {
				int i = Integer.parseInt(split[2]);
				return i;
			}
		}
		return null;
	}
}