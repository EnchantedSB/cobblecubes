package me.enchanted.cobblecubes.guis;

import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.enchanted.cobblecubes.CobbleCubes;
import me.enchanted.cobblecubes.listeners.PlaceGeneratorEvent;
import me.enchanted.cobblecubes.utils.PlayerData;

public class UpgradesGui implements Listener {

	public static void openInv(Player p, Location center) {
		PlayerData data = new PlayerData(p.getUniqueId());
		int lootl = 0;
		int storagel = 0;
		for (String keys : data.getConfig().getConfigurationSection("gens").getKeys(false)) {
			if (center
					.equals(new Location(Bukkit.getWorld(data.getConfig().getString("gens." + keys + ".center.world")),
							data.getConfig().getInt("gens." + keys + ".center.x"),
							data.getConfig().getInt("gens." + keys + ".center.y"),
							data.getConfig().getInt("gens." + keys + ".center.z")))) {
				lootl = data.getConfig().getInt("gens." + keys + ".loot");
				storagel = data.getConfig().getInt("gens." + keys + ".quantity");

			}
		}

		Inventory inv = Bukkit.createInventory(null, 27, "�7Upgrades");

		for (int i = 0; i < inv.getSize(); i++) {
			ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 1);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("�7");
			is.setItemMeta(im);

			inv.setItem(i, is);
		}

		String lc = CobbleCubes.getInstance().getConfig().getInt(
				"cost." + PlaceGeneratorEvent.type.get(PlaceGeneratorEvent.pCenter.get(p)) + "loot." + (lootl + 1)) == 0
						? "N/A"
						: String.format("%,d",
								CobbleCubes.getInstance().getConfig()
										.getInt("cost."
												+ PlaceGeneratorEvent.type.get(PlaceGeneratorEvent.pCenter.get(p))
												+ "loot." + (lootl + 1)));

		System.out.print(lc);
		ArrayList<String> ll = new ArrayList<String>();
		ll.add("�7");
		ll.add("�aLoot Upgrade �7allows you to �2increase the value");
		ll.add("�7of your �2ores generated �7inside the CobbleCube.");
		ll.add("�7");
		ll.add(" �2�l* �a�lCURRENT LEVEL�7: �r" + lootl);
		ll.add(" �2�l* �a�lNEXT UPGRADE COST�7: �r$" + lc);
		ItemStack loot = new ItemStack(Material.EMERALD, 1);
		ItemMeta lm = loot.getItemMeta();
		lm.setDisplayName("�2�l[!] �a�lLOOT UPGRADE �7(Click)");
		lm.setLore(ll);
		loot.setItemMeta(lm);

		String sc = CobbleCubes.getInstance().getConfig().getInt(
				"cost." + PlaceGeneratorEvent.type.get(PlaceGeneratorEvent.pCenter.get(p)) + "storage." + (storagel + 1)) == 0
						? "N/A"
						: String.format("%,d",
								CobbleCubes.getInstance().getConfig()
										.getInt("cost."
												+ PlaceGeneratorEvent.type.get(PlaceGeneratorEvent.pCenter.get(p))
												+ "storage." + (storagel + 1)));
		
		ArrayList<String> sl = new ArrayList<String>();
		sl.add("�7");
		sl.add("�eStorage Upgrade �7allows you to �6increase");
		sl.add("�7the �6amount of ores �7that can be stored at a time.");
		sl.add("�7");
		sl.add(" �6�l* �e�lCURRENT LEVEL�7: �r" + storagel);
		sl.add(" �6�l* �e�lNEXT UPGRADE COST�7: �r$" + sc);
		ItemStack storage = new ItemStack(Material.HOPPER, 1);
		ItemMeta sm = storage.getItemMeta();
		sm.setDisplayName("�6�l[!] �e�lSTORAGE UPGRADE �7(Click)");
		sm.setLore(sl);
		storage.setItemMeta(sm);

		inv.setItem(11, loot);
		inv.setItem(15, storage);

		p.openInventory(inv);
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if (e.getInventory().getName().equalsIgnoreCase("�7Upgrades")) {
			e.setCancelled(true);
			if(e.getSlot() == 11) {
				LootGui.openInv((Player) e.getWhoClicked(), PlaceGeneratorEvent.pCenter.get(e.getWhoClicked()));
			} else if(e.getSlot() == 15) {
				StorageGui.openInv((Player) e.getWhoClicked(), PlaceGeneratorEvent.pCenter.get(e.getWhoClicked()));
			}
		}
	}
}
